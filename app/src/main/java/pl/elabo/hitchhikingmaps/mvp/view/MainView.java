package pl.elabo.hitchhikingmaps.mvp.view;

import pl.elabo.hitchhikingmaps.mvp.view.base.BaseView;

/**
 * Created by michalkachel on 15.08.2015.
 */
public interface MainView extends BaseView {

	void initializeSlidingUpPanel();

	void initializeMap();

	void initializePlaceDetail();

	void expandSlidingUpPanel();

	void collapseSlidingUpPanel();

	boolean isPanelExpanded();

	void setSlidingUpPanelTouchEnabled(boolean enabled);

	void showPlaceDetail(long remoteId);

	void notifyPanelStateChanged(boolean isExpanded);
}
