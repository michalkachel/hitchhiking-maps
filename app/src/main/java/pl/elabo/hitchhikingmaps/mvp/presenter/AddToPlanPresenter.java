package pl.elabo.hitchhikingmaps.mvp.presenter;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface AddToPlanPresenter {

	void onViewCreated();

	void onNewPlan();

	void onPlanNameTextChanged(String planName);

	void onCreatePlan(String planName);

	void onCancel();

	void onAddToPlan(long planId);
}
