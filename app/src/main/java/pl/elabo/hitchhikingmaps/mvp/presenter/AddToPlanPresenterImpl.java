package pl.elabo.hitchhikingmaps.mvp.presenter;

import android.os.Handler;
import android.os.Looper;

import com.crashlytics.android.Crashlytics;

import java.util.List;

import pl.elabo.hitchhikingmaps.callback.plan.AddPlaceToPlanCallback;
import pl.elabo.hitchhikingmaps.callback.plan.CreateNewPlanCallback;
import pl.elabo.hitchhikingmaps.callback.plan.GetAllPlansCallback;
import pl.elabo.hitchhikingmaps.interactor.plan.AddPlaceToPlanUseCaseImpl;
import pl.elabo.hitchhikingmaps.interactor.plan.CreateNewPlanUseCaseImpl;
import pl.elabo.hitchhikingmaps.interactor.plan.GetAllPlansUseCaseImpl;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.BasePresenter;
import pl.elabo.hitchhikingmaps.mvp.view.AddToPlanView;

/**
 * Created by ELABO on 10.08.2015.
 */
public class AddToPlanPresenterImpl extends BasePresenter<AddToPlanView> implements AddToPlanPresenter, GetAllPlansCallback, CreateNewPlanCallback, AddPlaceToPlanCallback {

	private long mPlaceRemoteId;

	public AddToPlanPresenterImpl(AddToPlanView view, long placeRemoteId) {
		super(view);
		mPlaceRemoteId = placeRemoteId;
	}

	@Override
	public void onViewCreated() {
		loadAndShowAllPlans();
	}

	@Override
	public void onNewPlan() {
		getView().setPlansHolderVisibility(false);
		getView().setCreateNewPlanHolderVisibility(true);
		getView().setAddToPlanEnabled(false);
		getView().requestFocusAndCleanNewPlanName();
	}

	@Override
	public void onPlanNameTextChanged(String planName) {
		getView().setCreateNewPlanEnabled(planName.length() > 0);
	}

	@Override
	public void onCreatePlan(String planName) {
		new CreateNewPlanUseCaseImpl(this).execute(planName);
	}

	@Override
	public void onCancel() {
		getView().dismissDialog();
	}

	@Override
	public void onAddToPlan(long planId) {
		new AddPlaceToPlanUseCaseImpl(this).execute(planId, mPlaceRemoteId);
	}

	private void loadAndShowAllPlans() {
		new GetAllPlansUseCaseImpl(this).execute();
	}

	private void switchViewToCreatePlan() {
		getView().requestFocusAndCleanNewPlanName();
		getView().setPlansHolderVisibility(false);
		getView().setCreateNewPlanHolderVisibility(true);
		getView().setAddToPlanEnabled(false);
	}

	private void switchViewToChoosePlan() {
		getView().setPlansHolderVisibility(true);
		getView().setCreateNewPlanHolderVisibility(false);
		getView().setAddToPlanEnabled(true);
	}

	@Override
	public void onAllPlansReceived(final List<Plan> plans) {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (plans.size() > 0) {
					switchViewToChoosePlan();
					getView().showPlans(plans);
					getView().setNoPlansAvailableVisibility(false);
				} else {
					switchViewToCreatePlan();
					getView().setNoPlansAvailableVisibility(true);
				}
			}
		});
	}

	@Override
	public void onPlanCreated(Plan plan) {
		loadAndShowAllPlans();
		getView().setAddToPlanEnabled(true);
	}

	@Override
	public void onPlaceAddedToPlan(final Plan plan, final boolean alreadyInPlan) {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (alreadyInPlan) {
					getView().notifyPlaceAlreadyInPlan(plan.getName());
				} else {
					getView().showPlaceAddedToPlanMessage(plan.getName());
					getView().dismissDialog();
				}
			}
		});
	}

	@Override
	public void onError(final Throwable throwable) {
		Crashlytics.logException(throwable);
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				getView().showError(throwable);
			}
		});
	}
}
