package pl.elabo.hitchhikingmaps.mvp.presenter;

import android.os.Handler;
import android.os.Looper;

import com.crashlytics.android.Crashlytics;

import pl.elabo.hitchhikingmaps.callback.places.UpdateAllPlacesCallback;
import pl.elabo.hitchhikingmaps.interactor.places.UpdateAllPlacesUseCase;
import pl.elabo.hitchhikingmaps.interactor.places.UpdateAllPlacesUseCaseImpl;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.BasePresenter;
import pl.elabo.hitchhikingmaps.mvp.view.UpdateView;

/**
 * Created by ELABO on 10.08.2015.
 */
public class UpdatePresenterImpl extends BasePresenter<UpdateView> implements UpdatePresenter, UpdateAllPlacesCallback {

	private UpdateAllPlacesUseCase mUpdateAllPlacesUseCase;

	public UpdatePresenterImpl(UpdateView view) {
		super(view);
	}

	@Override
	public void onCreate() {
		getView().setScreenTitle();
	}

	@Override
	public void onUpdate() {
		if (getView().isNetworkEnabled()) {
			getView().showUpdateDialog();
			mUpdateAllPlacesUseCase = new UpdateAllPlacesUseCaseImpl(this);
			mUpdateAllPlacesUseCase.execute();
		} else {
			getView().notifyInternetConnectionNotAvailable();
		}
	}

	@Override
	public void onUpdateInterrupted() {
		mUpdateAllPlacesUseCase.interrupt();
	}

	@Override
	public void onError(final Throwable throwable) {
		Crashlytics.logException(throwable);
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				getView().showError(throwable);
			}
		});
	}

	@Override
	public void onPlacesUpdated(final int addedPlacesCounter, final boolean wasInterrupted) {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (getView() != null) {
					getView().dismissUpdateDialog();
					getView().notifyPlacesAdded(addedPlacesCounter);
					if (!wasInterrupted) {
						getView().disableUpdate();
					}
				}
			}
		});
	}
}
