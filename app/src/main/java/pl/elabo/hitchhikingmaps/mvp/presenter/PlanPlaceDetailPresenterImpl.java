package pl.elabo.hitchhikingmaps.mvp.presenter;

import android.os.Handler;
import android.os.Looper;

import pl.elabo.hitchhikingmaps.callback.plan.DeletePlaceFromPlanCallback;
import pl.elabo.hitchhikingmaps.interactor.plan.DeletePlaceFromPlanUseCaseImpl;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.BasePlaceDetailPresenter;
import pl.elabo.hitchhikingmaps.mvp.view.PlanPlaceDetailView;

/**
 * Created by ELABO on 10.08.2015.
 */
public class PlanPlaceDetailPresenterImpl extends BasePlaceDetailPresenter<PlanPlaceDetailView> implements PlanPlaceDetailPresenter, DeletePlaceFromPlanCallback {

	private long mPlanId;

	public PlanPlaceDetailPresenterImpl(PlanPlaceDetailView view, long planId) {
		super(view);
		mPlanId = planId;
	}

	@Override
	public void onRemoveFromPlan() {
		getView().askForPlaceRemoval();
	}

	@Override
	public void onRemoveFromPlanConfirmed() {
		new DeletePlaceFromPlanUseCaseImpl(this).execute(mPlanId, mPlaceRemoteId);
	}

	@Override
	public void onPlaceDeletedFromPlan(final Plan plan, long placeRemoteId) {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (getView() != null) {
					getView().showPlaceRemovalConfirmation();
					getView().setPanelTouchEnabled(plan.getPlacesCount() != 0);
					if (plan.getPlacesCount() == 0) {
						getView().hidePlaceDetailHolder();
					}
				}
			}
		});
		getView().removeRemovedPlaceFromMap(plan.getId(), placeRemoteId);
	}
}
