package pl.elabo.hitchhikingmaps.mvp.presenter.base;

import org.osmdroid.api.IGeoPoint;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface MapPresenter {

	void onViewCreated();

	void onResume();

	void onPause();

	void onDestroyView();

	void onShowMyLocation();

	void onEnableLocation();

	void onPlaceSelected(long remoteId, IGeoPoint geoPoint);

}
