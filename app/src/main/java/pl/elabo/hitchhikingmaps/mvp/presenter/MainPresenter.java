package pl.elabo.hitchhikingmaps.mvp.presenter;

/**
 * Created by michalkachel on 15.08.2015.
 */
public interface MainPresenter {

	void onCreate();

	void onExpandCollapsePanelSelected(boolean isExpaned);

	void onPanelTouchEnabled(boolean enabled);

	void onPanelStateChanged(boolean isExpaned);

	void onPlaceSelected(long remoteId);

}
