package pl.elabo.hitchhikingmaps.mvp.presenter;

import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface PlansListPresenter {

	void onViewCreated();

	void onResume();

	void onPlanSelected(Plan plan);

}
