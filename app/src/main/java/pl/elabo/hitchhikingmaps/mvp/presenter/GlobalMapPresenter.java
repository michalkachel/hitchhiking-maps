package pl.elabo.hitchhikingmaps.mvp.presenter;

import org.osmdroid.util.BoundingBoxE6;

import pl.elabo.hitchhikingmaps.mvp.presenter.base.MapPresenter;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface GlobalMapPresenter extends MapPresenter {

	void onMapChanged(BoundingBoxE6 boundingBox);

	void onRefreshMapNow(BoundingBoxE6 boundingBoxE6);

}
