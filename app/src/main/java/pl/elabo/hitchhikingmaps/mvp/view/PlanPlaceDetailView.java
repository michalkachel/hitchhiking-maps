package pl.elabo.hitchhikingmaps.mvp.view;

import pl.elabo.hitchhikingmaps.mvp.view.base.BasePlaceDetailView;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface PlanPlaceDetailView extends BasePlaceDetailView {

	void askForPlaceRemoval();

	void showPlaceRemovalConfirmation();

	void removeRemovedPlaceFromMap(long planId, long remotePlaceId);

}
