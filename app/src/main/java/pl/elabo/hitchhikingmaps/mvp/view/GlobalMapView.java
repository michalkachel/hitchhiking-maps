package pl.elabo.hitchhikingmaps.mvp.view;

import pl.elabo.hitchhikingmaps.mvp.view.base.BaseMapView;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface GlobalMapView extends BaseMapView {

	void initializeMapChangeListener();

	void invokeDelayedOnMapChanged(long delayInMillis);

}
