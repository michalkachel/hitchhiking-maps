package pl.elabo.hitchhikingmaps.mvp.view;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import pl.elabo.hitchhikingmaps.mvp.view.base.BaseView;

/**
 * Created by michalkachel on 15.08.2015.
 */
public interface SplashView extends BaseView {

	boolean wasFirstUpdate();

	void navigateToGlobalMap();

	void setWasFirstUpdate(boolean wasFirstUpdate);

	void showProgress();

	List<InputStream> getJsonInputs() throws IOException;

	List<String> getContinentsNames();

	void updateProgress(String continent, int progress, int max);

	void showTutorial();

	void showShowMapButton();

	boolean isTutorialShowing();

}
