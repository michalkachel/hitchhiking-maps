package pl.elabo.hitchhikingmaps.mvp.presenter;

import android.os.Handler;
import android.os.Looper;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okio.Okio;
import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.callback.places.InitialInsertAllPlacesCallback;
import pl.elabo.hitchhikingmaps.interactor.places.InitialInsertAllPlacesUseCase;
import pl.elabo.hitchhikingmaps.interactor.places.InitialInsertAllPlacesUseCaseImpl;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.BasePresenter;
import pl.elabo.hitchhikingmaps.mvp.view.SplashView;

/**
 * Created by ELABO on 10.08.2015.
 */
public class SplashPresenterImpl extends BasePresenter<SplashView> implements SplashPresenter, InitialInsertAllPlacesCallback {
	private Handler mHandler = new Handler();
	private InitialInsertAllPlacesUseCase mInitialInsertAllPlacesUseCase;

	private List<String> mContinents;

	private Runnable mSplashRunnable = new Runnable() {
		@Override
		public void run() {
			if (getView() != null) {
				getView().navigateToGlobalMap();
			}
		}
	};

	public SplashPresenterImpl(SplashView view) {
		super(view);
	}

	@Override
	public void onCreate() {
		if (!getView().wasFirstUpdate()) {
			mContinents = getView().getContinentsNames();
			mInitialInsertAllPlacesUseCase = InitialInsertAllPlacesUseCaseImpl.getInstance();
			mInitialInsertAllPlacesUseCase.setJsonStrings(getJsonStrings());
			mInitialInsertAllPlacesUseCase.execute();
			mInitialInsertAllPlacesUseCase.setCallbackReference(this);
			getView().showProgress();
		}
	}

	@Override
	public void onResume() {
		if (getView().wasFirstUpdate()) {
			mHandler.postDelayed(mSplashRunnable, AppConstants.SPLASH_DELAY);
		}
	}

	private List<String> getJsonStrings() {
		List<String> jsonStrings = new ArrayList<>();
		try {
			List<InputStream> jsonInputs = getView().getJsonInputs();
			for (InputStream inputStream : jsonInputs) {
				jsonStrings.add(Okio.buffer(Okio.source(inputStream)).readByteString().utf8());
			}
		} catch (IOException e) {
			e.printStackTrace();
			onError(e);
		}
		return jsonStrings;
	}

	@Override
	public void onPause() {
		mHandler.removeCallbacks(mSplashRunnable);
	}

	@Override
	public void onDestroy() {
		mInitialInsertAllPlacesUseCase = InitialInsertAllPlacesUseCaseImpl.getInstance();
		mInitialInsertAllPlacesUseCase.removeCallbackReference();
	}

	@Override
	public void onShowTutorial() {
		getView().showTutorial();
	}

	@Override
	public void onFragmentDetached() {
		if (getView().wasFirstUpdate()) {
			getView().navigateToGlobalMap();
		}
	}

	@Override
	public void onAllPlacesInserted() {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				getView().setWasFirstUpdate(true);
				if (!getView().isTutorialShowing()) {
					getView().navigateToGlobalMap();
				} else {
					getView().showShowMapButton();
				}
			}
		});
	}

	@Override
	public void onProgressChanged(final int index, final int progress, final int max) {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (getView() != null) {
					getView().updateProgress(mContinents.get(index), progress, max);
				}
			}
		});

	}

	@Override
	public void onError(final Throwable throwable) {
		Crashlytics.logException(throwable);
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				getView().showError(throwable);
			}
		});
	}
}
