package pl.elabo.hitchhikingmaps.mvp.presenter;

import android.os.Handler;

import com.crashlytics.android.Crashlytics;

import org.osmdroid.util.BoundingBoxE6;

import java.util.List;

import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.callback.places.GetPlacesFromBoundingBoxCallback;
import pl.elabo.hitchhikingmaps.interactor.places.GetPLacesFromBoundingBoxUseCaseImpl;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.BaseMapPresenter;
import pl.elabo.hitchhikingmaps.mvp.view.GlobalMapView;

/**
 * Created by ELABO on 10.08.2015.
 */
public class GlobalMapPresenterImpl extends BaseMapPresenter<GlobalMapView> implements GlobalMapPresenter, GetPlacesFromBoundingBoxCallback {

	public static final long LOAD_SPOTS_AFTER_LOCATION_INVOKE_DELAY = 3000;

	private BoundingBoxE6 mBoundingBoxE6;
	private final Runnable mRunnable = new Runnable() {
		@Override
		public void run() {
			refreshMap();
		}
	};
	private Handler mHandler = new Handler();

	public GlobalMapPresenterImpl(GlobalMapView view) {
		super(view);
	}

	@Override
	public void onViewCreated() {
		super.onViewCreated();
		getView().initializeMapChangeListener();
		getView().invokeDelayedOnMapChanged(LOAD_SPOTS_AFTER_LOCATION_INVOKE_DELAY);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mHandler.removeCallbacksAndMessages(null);
	}

	@Override
	public void onShowMyLocation() {
		super.onShowMyLocation();
		if (getView().isLocationEnabled()) {
			getView().invokeDelayedOnMapChanged(LOAD_SPOTS_AFTER_LOCATION_INVOKE_DELAY);
		}
	}

	@Override
	public void onMapChanged(BoundingBoxE6 boundingBox) {
		mBoundingBoxE6 = boundingBox;
		mHandler.removeCallbacks(mRunnable);
		mHandler.postDelayed(mRunnable, AppConstants.Map.SCROLL_MAP_MARKERS_LOADING_DELAY);
	}

	@Override
	public void onRefreshMapNow(BoundingBoxE6 boundingBoxE6) {
		mBoundingBoxE6 = boundingBoxE6;
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				refreshMap();
			}
		});
	}

	private void refreshMap() {
		//// 27.07.2015 east and west is swapped in mapview bounding box
		new GetPLacesFromBoundingBoxUseCaseImpl(this).execute(mBoundingBoxE6.getLatNorthE6(), mBoundingBoxE6.getLatSouthE6(), mBoundingBoxE6.getLonEastE6(), mBoundingBoxE6.getLonWestE6());
	}

	@Override
	public void onPlacesFromBoundingBoxReceived(List<Place> places) {
		getView().showPlaces(places);
	}

	@Override
	public void onError(Throwable throwable) {
		Crashlytics.logException(throwable);
		getView().showError(throwable);
	}
}
