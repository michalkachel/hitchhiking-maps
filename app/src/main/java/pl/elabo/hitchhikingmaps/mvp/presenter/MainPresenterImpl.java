package pl.elabo.hitchhikingmaps.mvp.presenter;

import pl.elabo.hitchhikingmaps.mvp.presenter.base.BasePresenter;
import pl.elabo.hitchhikingmaps.mvp.view.MainView;

/**
 * Created by ELABO on 10.08.2015.
 */
public class MainPresenterImpl extends BasePresenter<MainView> implements MainPresenter {

	public MainPresenterImpl(MainView view) {
		super(view);
	}

	@Override
	public void onCreate() {
		getView().initializeMap();
		getView().initializePlaceDetail();
		getView().initializeSlidingUpPanel();
	}

	@Override
	public void onExpandCollapsePanelSelected(boolean isExpaned) {
		if (isExpaned) {
			getView().collapseSlidingUpPanel();
		} else {
			getView().expandSlidingUpPanel();
		}
	}

	@Override
	public void onPanelTouchEnabled(boolean enabled) {
		getView().setSlidingUpPanelTouchEnabled(enabled);
		if (!enabled && getView().isPanelExpanded()) {
			getView().collapseSlidingUpPanel();
		}
	}

	@Override
	public void onPanelStateChanged(boolean isExpaned) {
		getView().notifyPanelStateChanged(isExpaned);
	}

	@Override
	public void onPlaceSelected(long remoteId) {
		getView().showPlaceDetail(remoteId);
	}
}
