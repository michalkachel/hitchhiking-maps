package pl.elabo.hitchhikingmaps.mvp.presenter;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface SplashPresenter {

	void onCreate();

	void onResume();

	void onPause();

	void onDestroy();

	void onShowTutorial();

	void onFragmentDetached();

}
