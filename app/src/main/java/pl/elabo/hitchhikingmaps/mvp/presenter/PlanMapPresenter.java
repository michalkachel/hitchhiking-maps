package pl.elabo.hitchhikingmaps.mvp.presenter;

import pl.elabo.hitchhikingmaps.mvp.presenter.base.MapPresenter;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface PlanMapPresenter extends MapPresenter {

	void onPlaceRemovedFromPlan(long planId, long placeRemoteId);

	void onDownloadMap();

	void onDeletePlan();

	void onConfirmPlanDeleting();

}
