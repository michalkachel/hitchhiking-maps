package pl.elabo.hitchhikingmaps.mvp.presenter.base;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface PlaceDetailPresenter {

	void onViewCreated();

	void onPlaceSelected(long remoteId);

	void onTogglePanelSelected();

	void onPanelStateChanged(boolean isExpanded);

	void onShowStreetView();

}
