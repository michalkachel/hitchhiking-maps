package pl.elabo.hitchhikingmaps.mvp.presenter;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface UpdatePresenter {

	void onCreate();

	void onUpdate();

	void onUpdateInterrupted();

}
