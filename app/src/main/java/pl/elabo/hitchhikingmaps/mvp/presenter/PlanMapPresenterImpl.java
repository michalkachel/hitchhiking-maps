package pl.elabo.hitchhikingmaps.mvp.presenter;

import android.os.Handler;
import android.os.Looper;

import com.crashlytics.android.Crashlytics;

import java.util.List;

import pl.elabo.hitchhikingmaps.callback.plan.DeletePlanCallback;
import pl.elabo.hitchhikingmaps.callback.plan.GetPlanCallback;
import pl.elabo.hitchhikingmaps.interactor.plan.DeletePlanUseCaseImpl;
import pl.elabo.hitchhikingmaps.interactor.plan.GetPlanUseCaseImpl;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.BaseMapPresenter;
import pl.elabo.hitchhikingmaps.mvp.view.PlanMapView;
import pl.elabo.hitchhikingmaps.util.LocationUtil;

/**
 * Created by ELABO on 10.08.2015.
 */
public class PlanMapPresenterImpl extends BaseMapPresenter<PlanMapView> implements PlanMapPresenter, GetPlanCallback, DeletePlanCallback {

	private long mPlanId;
	private Plan mPlan;

	public PlanMapPresenterImpl(PlanMapView view, long planId) {
		super(view);
		mPlanId = planId;
	}

	@Override
	public void onViewCreated() {
		super.onViewCreated();
		new GetPlanUseCaseImpl(this, mPlanId).execute();
	}

	private void selectFirstPlaceInPlan(Plan plan) {
		if (plan != null && plan.getPlacesCount() > 0) {
			final Place place = plan.getPlaces().get(0);
			getView().showPlaceDetails(place.getRemoteId());
			getView().markPointOnMap(LocationUtil.placeToGeoPoint(place), false);
		}
	}

	@Override
	public void onPlanReceived(final Plan plan) {
		if (plan != null) {
			plan.loadPlaces();
			mPlan = plan;
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					if (getView() != null) {
						final List<Place> places = plan.getPlaces();
						getView().setTitle(plan.getName());
						getView().setDownloadMapButtonVisibility(!plan.isMapDownloaded() && places.size() != 0);
						getView().showPlaces(places);
						if (places.size() > 0) {
							getView().zoomToBoundingBox(LocationUtil.getBoundingBox(places, true));
							selectFirstPlaceInPlan(plan);
						} else {
							getView().showMapWithoutPlanPlaces();
						}
					}
				}
			});
		} else {
			getView().closePlan();
		}
	}

	@Override
	public void onPlaceRemovedFromPlan(long planId, long placeRemoteId) {
		new GetPlanUseCaseImpl(this, mPlanId).execute();
	}

	@Override
	public void onDownloadMap() {
		if (!mPlan.isMapDownloaded()) {
			if (getView().isNetworkEnabled()) {
				getView().downloadMap(mPlan.getId());
				getView().notifyMapStartedToDownload();
				getView().setDownloadMapButtonVisibility(false);
			} else {
				getView().notifyInternetConnectionNotAvailable();
			}
		} else {
			getView().notifyMapAlreadyDownloaded();
			getView().setDownloadMapButtonVisibility(false);
		}
	}

	@Override
	public void onDeletePlan() {
		getView().askForPlanDeleting();
	}

	@Override
	public void onConfirmPlanDeleting() {
		new DeletePlanUseCaseImpl(this).execute(mPlan);
	}

	@Override
	public void onPlanDeleted(String planName) {
		getView().notifyPlanDeleted(planName);
		getView().closePlan();
	}

	@Override
	public void onError(final Throwable throwable) {
		Crashlytics.logException(throwable);
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (getView() != null) {
					getView().showError(throwable);
				}
			}
		});
	}
}
