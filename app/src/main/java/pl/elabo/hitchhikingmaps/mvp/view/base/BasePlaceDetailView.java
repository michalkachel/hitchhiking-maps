package pl.elabo.hitchhikingmaps.mvp.view.base;

import java.util.Collection;
import java.util.List;

import pl.elabo.hitchhikingmaps.model.entity.Comment;
import pl.elabo.hitchhikingmaps.model.entity.PlaceDescription;
import pl.elabo.hitchhikingmaps.model.entity.WaitingStatistic;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface BasePlaceDetailView extends BaseView {

	void togglePanelState();

	void showPlaceDetailHolder();

	void hidePlaceDetailHolder();

	void showProgress();

	void hideProgress();

	void setPanelTouchEnabled(boolean enabled);

	void setTogglePanelText(boolean isExpanded);

	void showRating(int rating);

	void showLocationLocality(String locality);

	void hideLocationLocality();

	void showLocationCountryAndContinent(String countryAndContinent);

	void hideLocationCountryAndContinent();

	void showRatingCount(String ratingCount);

	void showCommentsCount(String commentsCount);

	void showWaitingStatistic(WaitingStatistic waitingStatistic);

	void hideWaitingStatistic();

	void showDescriptions(Collection<PlaceDescription> descriptions);

	void hideDescriptionsHolder();

	void showComments(List<Comment> comments);

	void hideCommentsHolder();

	boolean isNetworkEnabled();

	void notifyInternetConnectionNotAvailable();

	void navigateToStreetView(double latitude, double longitude);

}
