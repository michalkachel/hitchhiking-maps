package pl.elabo.hitchhikingmaps.mvp.presenter.base;

import org.osmdroid.api.IGeoPoint;

import pl.elabo.hitchhikingmaps.mvp.view.base.BaseMapView;

/**
 * Created by ELABO on 10.08.2015.
 */
public abstract class BaseMapPresenter<T extends BaseMapView> extends BasePresenter<T> implements MapPresenter {

	protected BaseMapPresenter(T view) {
		super(view);
	}

	@Override
	public void onViewCreated() {
		getView().initializeMap();
		getView().initializeMyLocation();
	}

	@Override
	public void onDestroyView() {

	}

	@Override
	public void onResume() {
		getView().enableMyLocation();
	}

	@Override
	public void onPause() {
		getView().disableMyLocation();
	}

	@Override
	public void onShowMyLocation() {
		if (getView().isLocationEnabled()) {
			getView().enableMyLocation();
			getView().followMyLocation();
			if (!getView().isLocationDetected()) {
				getView().showNoLocationDetected();
			}
		} else {
			getView().askForLocationEnabling();
		}
	}

	@Override
	public void onEnableLocation() {
		getView().navigateToLocationSettings();
	}

	@Override
	public void onPlaceSelected(long remoteId, IGeoPoint geoPoint) {
		getView().showPlaceDetails(remoteId);
		getView().markPointOnMap(geoPoint, true);
	}
}
