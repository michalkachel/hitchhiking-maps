package pl.elabo.hitchhikingmaps.mvp.view;

import java.util.List;

import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.mvp.view.base.BaseView;

public interface PlansListView extends BaseView {

	void initList();

	void showPlans(List<Plan> plans);

	void navigateToPlan(Plan plan);

	void setEmptyPlansVisibility(boolean visible);

}
