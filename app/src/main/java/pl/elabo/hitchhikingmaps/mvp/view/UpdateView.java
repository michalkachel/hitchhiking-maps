package pl.elabo.hitchhikingmaps.mvp.view;

import pl.elabo.hitchhikingmaps.mvp.view.base.BaseView;

/**
 * Created by michalkachel on 15.08.2015.
 */
public interface UpdateView extends BaseView {

	void setScreenTitle();

	void showUpdateDialog();

	void dismissUpdateDialog();

	void notifyPlacesAdded(int addedPlacesCounter);

	void disableUpdate();

	boolean isNetworkEnabled();

	void notifyInternetConnectionNotAvailable();

}
