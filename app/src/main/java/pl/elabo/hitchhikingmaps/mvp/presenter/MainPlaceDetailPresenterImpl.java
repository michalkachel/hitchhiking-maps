package pl.elabo.hitchhikingmaps.mvp.presenter;

import pl.elabo.hitchhikingmaps.mvp.presenter.base.BasePlaceDetailPresenter;
import pl.elabo.hitchhikingmaps.mvp.view.MainPlaceDetailView;

/**
 * Created by ELABO on 10.08.2015.
 */
public class MainPlaceDetailPresenterImpl extends BasePlaceDetailPresenter<MainPlaceDetailView> implements MainPlaceDetailPresenter {

	public MainPlaceDetailPresenterImpl(MainPlaceDetailView view) {
		super(view);
	}

	@Override
	public void onAddToPlan() {
		getView().showAddToPlanDialog(mPlaceRemoteId);
	}

}
