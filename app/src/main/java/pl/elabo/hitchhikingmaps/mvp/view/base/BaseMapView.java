package pl.elabo.hitchhikingmaps.mvp.view.base;

import org.osmdroid.api.IGeoPoint;

import java.util.List;

import pl.elabo.hitchhikingmaps.model.entity.Place;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface BaseMapView extends BaseView {

	void initializeMap();

	void initializeMyLocation();

	void showPlaces(List<Place> places);

	void markPointOnMap(IGeoPoint geoPoint, boolean animateTo);

	boolean isLocationEnabled();

	boolean isLocationDetected();

	void enableMyLocation();

	void followMyLocation();

	void disableMyLocation();

	void showNoLocationDetected();

	void askForLocationEnabling();

	void navigateToLocationSettings();

	void showPlaceDetails(long remoteId);
}
