package pl.elabo.hitchhikingmaps.mvp.view.base;

/**
 * Created by michalkachel on 20.07.2015.
 */
public interface BaseView {

	void showError(Throwable throwable);

}
