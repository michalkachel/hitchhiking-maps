package pl.elabo.hitchhikingmaps.mvp.presenter.base;

import android.os.Handler;
import android.os.Looper;

import com.crashlytics.android.Crashlytics;

import java.util.List;
import java.util.Map;

import pl.elabo.hitchhikingmaps.callback.places.GetPlaceDetailsCallback;
import pl.elabo.hitchhikingmaps.interactor.places.GetPlaceDetailsUseCaseImpl;
import pl.elabo.hitchhikingmaps.model.entity.Comment;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import pl.elabo.hitchhikingmaps.model.entity.PlaceDescription;
import pl.elabo.hitchhikingmaps.model.entity.PlaceLocationDescription;
import pl.elabo.hitchhikingmaps.model.entity.RatingStatistic;
import pl.elabo.hitchhikingmaps.model.entity.WaitingStatistic;
import pl.elabo.hitchhikingmaps.mvp.view.base.BasePlaceDetailView;

/**
 * Created by ELABO on 10.08.2015.
 */
public abstract class BasePlaceDetailPresenter<T extends BasePlaceDetailView> extends BasePresenter<T> implements PlaceDetailPresenter,
		GetPlaceDetailsCallback {

	protected long mPlaceRemoteId;
	protected Place mPlace;

	public BasePlaceDetailPresenter(T view) {
		super(view);
	}

	@Override
	public void onViewCreated() {
		getView().setPanelTouchEnabled(false);
	}

	@Override
	public void onPlaceSelected(long remoteId) {
		getView().showProgress();
		mPlaceRemoteId = remoteId;
		new GetPlaceDetailsUseCaseImpl(this).execute(remoteId);
	}

	@Override
	public void onTogglePanelSelected() {
		getView().togglePanelState();
	}

	@Override
	public void onShowStreetView() {
		if (getView().isNetworkEnabled()) {
			getView().navigateToStreetView(mPlace.getLatitude(), mPlace.getLongitude());
		} else {
			getView().notifyInternetConnectionNotAvailable();
		}
	}

	@Override
	public void onPanelStateChanged(boolean isExpanded) {
		getView().setTogglePanelText(isExpanded);
	}

	@Override
	public void onPlaceDetailsReceived(final Place place) {
		mPlace = place;
		if (getView() != null) {
			new Handler(Looper.getMainLooper()).post(new Runnable() {
				@Override
				public void run() {
					getView().showPlaceDetailHolder();
					if (place != null) {
						handlePlaceRating(place.getRating());
						handlePlaceLocationDescription(place.getPlaceLocationDescription());
						handlePlaceRatingStats(place.getRatingStatistic());
						handleComments(place.getComments());
						handleWaitingStats(place.getWaitingStatistic());
						handleDescription(place.getDescriptionsMap());
					}
					getView().setPanelTouchEnabled(true);
					getView().hideProgress();
				}
			});
		}
	}

	private void handlePlaceRating(int rating) {
		getView().showRating(countRealRating(rating));
	}

	/*Rating from 0 - 5
	* 0 - no rating
	* 1 - very good
	* 5 - senseless*/
	private int countRealRating(int rating) {
		if (rating == 0) {
			return rating;
		} else {
			return 6 - rating;
		}
	}

	private void handlePlaceLocationDescription(PlaceLocationDescription placeLocationDescription) {
		if (placeLocationDescription != null) {
			final String locality = placeLocationDescription.getLocality();
			if (locality != null) {
				getView().showLocationLocality(locality);
			} else {
				getView().hideLocationLocality();
			}

			getView().showLocationCountryAndContinent(placeLocationDescription.getCountry().getName() + ", " + placeLocationDescription.getContinent().getName());
		} else {
			getView().hideLocationLocality();
			getView().hideLocationCountryAndContinent();
		}
	}

	private void handlePlaceRatingStats(RatingStatistic ratingStatistic) {
		if (ratingStatistic != null) {
			getView().showRatingCount(String.valueOf(ratingStatistic.getRatingCount()));
		} else {
			getView().showRatingCount("-");
		}
	}

	private void handleComments(List<Comment> comments) {
		if (comments != null && comments.size() > 0) {
			getView().showCommentsCount(String.valueOf(comments.size()));
			getView().showComments(comments);
		} else {
			getView().showCommentsCount("-");
			getView().hideCommentsHolder();
		}
	}

	private void handleWaitingStats(WaitingStatistic waitingStatistic) {
		if (waitingStatistic != null) {
			getView().showWaitingStatistic(waitingStatistic);
		} else {
			getView().hideWaitingStatistic();
		}
	}

	private void handleDescription(Map<String, PlaceDescription> placeDescriptionMap) {
		if (placeDescriptionMap != null && placeDescriptionMap.size() > 0) {
			getView().showDescriptions(placeDescriptionMap.values());
		} else {
			getView().hideDescriptionsHolder();
		}
	}

	@Override
	public void onError(Throwable throwable) {
		Crashlytics.logException(throwable);
		getView().hideProgress();
		getView().showError(throwable);
	}
}
