package pl.elabo.hitchhikingmaps.mvp.view;

import org.osmdroid.util.BoundingBoxE6;

import pl.elabo.hitchhikingmaps.mvp.view.base.BaseMapView;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface PlanMapView extends BaseMapView {

	void setTitle(String title);

	void zoomToBoundingBox(BoundingBoxE6 boundingBoxE6);

	void setDownloadMapButtonVisibility(boolean visible);

	void askForPlanDeleting();

	void notifyPlanDeleted(String planName);

	void notifyMapStartedToDownload();

	void notifyMapAlreadyDownloaded();

	void notifyInternetConnectionNotAvailable();

	void showMapWithoutPlanPlaces();

	void downloadMap(long planId);

	boolean isNetworkEnabled();

	void closePlan();

}
