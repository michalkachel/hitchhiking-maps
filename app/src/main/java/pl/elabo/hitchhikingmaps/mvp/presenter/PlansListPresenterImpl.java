package pl.elabo.hitchhikingmaps.mvp.presenter;

import android.os.Handler;
import android.os.Looper;

import com.crashlytics.android.Crashlytics;

import java.util.List;

import pl.elabo.hitchhikingmaps.callback.plan.GetAllPlansCallback;
import pl.elabo.hitchhikingmaps.interactor.plan.GetAllPlansUseCaseImpl;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.BasePresenter;
import pl.elabo.hitchhikingmaps.mvp.view.PlansListView;

/**
 * Created by ELABO on 10.08.2015.
 */
public class PlansListPresenterImpl extends BasePresenter<PlansListView> implements PlansListPresenter,
		GetAllPlansCallback {

	public PlansListPresenterImpl(PlansListView view) {
		super(view);
	}

	@Override
	public void onViewCreated() {
		getView().initList();
	}

	@Override
	public void onResume() {
		new GetAllPlansUseCaseImpl(this).execute();
	}

	@Override
	public void onPlanSelected(Plan plan) {
		getView().navigateToPlan(plan);
	}

	@Override
	public void onAllPlansReceived(final List<Plan> plans) {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (getView() != null) {
					getView().showPlans(plans);
					getView().setEmptyPlansVisibility(plans.size() == 0);
				}
			}
		});
	}

	@Override
	public void onError(final Throwable throwable) {
		Crashlytics.logException(throwable);
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				if (getView() != null) {
					getView().showError(throwable);
				}
			}
		});
	}
}
