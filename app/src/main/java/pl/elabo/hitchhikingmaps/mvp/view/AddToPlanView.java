package pl.elabo.hitchhikingmaps.mvp.view;

import java.util.List;

import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.mvp.view.base.BaseView;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface AddToPlanView extends BaseView {

	void dismissDialog();

	void setNoPlansAvailableVisibility(boolean visible);

	void setCreateNewPlanHolderVisibility(boolean visible);

	void setPlansHolderVisibility(boolean visible);

	void showPlans(List<Plan> plans);

	void setCreateNewPlanEnabled(boolean enabled);

	void setAddToPlanEnabled(boolean enabled);

	void requestFocusAndCleanNewPlanName();

	void showPlaceAddedToPlanMessage(String planName);

	void notifyPlaceAlreadyInPlan(String planName);

}
