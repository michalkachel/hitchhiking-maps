package pl.elabo.hitchhikingmaps.mvp.presenter.base;

import pl.elabo.hitchhikingmaps.mvp.view.base.BaseView;

/**
 * Created by michalkachel on 20.07.2015.
 */
public abstract class BasePresenter<T extends BaseView> {
	private T mView;

	public BasePresenter(T view) {
		mView = view;
	}

	protected T getView() {
		return mView;
	}

}
