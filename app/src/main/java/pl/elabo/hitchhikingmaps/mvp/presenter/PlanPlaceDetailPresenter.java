package pl.elabo.hitchhikingmaps.mvp.presenter;

import pl.elabo.hitchhikingmaps.mvp.presenter.base.PlaceDetailPresenter;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface PlanPlaceDetailPresenter extends PlaceDetailPresenter {

	void onRemoveFromPlan();

	void onRemoveFromPlanConfirmed();

}
