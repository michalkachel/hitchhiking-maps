package pl.elabo.hitchhikingmaps.application;

/**
 * Created by michalkachel on 10.08.2015.
 */
public interface AppConstants {

	int SPLASH_DELAY = 2000;

	interface Map {
		int SCROLL_MAP_MARKERS_LOADING_DELAY = 500;
		int GLOBAL_MIN_ZOOM_LEVEL = 7;
		int PLAN_MIN_ZOOM_LEVEL = 4;
		int GLOBAL_INITIAL_ZOOM_LEVEL = 7;
		double GLOBAL_INITIAL_LATITUDE = 50.0;
		double GLOBAL_INITIAL_LONGITUDE = 20.0;
	}

	interface Extras {
		String PLACE_REMOTE_ID = "place_remote_id";
		String PLAN_ID = "plan_id";
		String LATITUDE = "latitude";
		String LONGITUDE = "longitude";
	}

}
