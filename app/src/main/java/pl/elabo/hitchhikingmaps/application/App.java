package pl.elabo.hitchhikingmaps.application;

import android.app.Application;
import android.os.StrictMode;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import io.fabric.sdk.android.Fabric;
import pl.elabo.hitchhikingmaps.BuildConfig;
import pl.elabo.hitchhikingmaps.R;
import timber.log.Timber;

/**
 * Created by michalkachel on 16.06.2015.
 */
public class App extends Application {

	private Tracker mTracker;

	@Override
	public void onCreate() {
		if (BuildConfig.DEBUG) {
			//LeakCanary.install(this);
			Timber.plant(new Timber.DebugTree());

			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
					.detectAll()
					.penaltyLog()
					.build());
			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
					.detectLeakedSqlLiteObjects()
					.penaltyLog()
					.penaltyDeath()
					.build());
		} else {
			Fabric.with(this, new Crashlytics());
		}
		super.onCreate();

		ActiveAndroid.initialize(this);

		getDefaultTracker();
	}

	synchronized public Tracker getDefaultTracker() {
		if (mTracker == null) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			mTracker = analytics.newTracker(R.xml.global_tracker);
			mTracker.enableAdvertisingIdCollection(true);
		}
		return mTracker;
	}

}
