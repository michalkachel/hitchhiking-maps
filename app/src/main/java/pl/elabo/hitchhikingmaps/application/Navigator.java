package pl.elabo.hitchhikingmaps.application;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import pl.elabo.hitchhikingmaps.ui.activity.AboutActivity;
import pl.elabo.hitchhikingmaps.ui.activity.MainActivity;
import pl.elabo.hitchhikingmaps.ui.activity.PlanMapActivity;
import pl.elabo.hitchhikingmaps.ui.activity.PlansActivity;
import pl.elabo.hitchhikingmaps.ui.activity.StreetViewActivity;
import pl.elabo.hitchhikingmaps.ui.activity.TutorialActivity;
import pl.elabo.hitchhikingmaps.ui.activity.UpdateActivity;

/**
 * Created by michalkachel on 22.07.2015.
 */
public class Navigator {

	public static void startMapActivity(Context context) {
		Intent intent = new Intent(context, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(intent);
	}

	public static void startPlansActivity(Context context) {
		Intent intent = new Intent(context, PlansActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(intent);
	}

	public static void startPlanMap(Context context, long planId) {
		context.startActivity(getPlanMapIntent(context, planId, false));
	}

	public static Intent getPlanMapIntent(Context context, long planId, boolean clearTop) {
		Intent intent = new Intent(context, PlanMapActivity.class);
		intent.putExtra(AppConstants.Extras.PLAN_ID, planId);
		if (clearTop) {
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		}
		return intent;
	}

	public static void startStreetViewActivity(Context context, double latitude, double longitude) {
		Intent intent = new Intent(context, StreetViewActivity.class);
		intent.putExtra(AppConstants.Extras.LONGITUDE, longitude);
		intent.putExtra(AppConstants.Extras.LATITUDE, latitude);
		context.startActivity(intent);
	}

	public static void startUpdateActivity(Context context) {
		context.startActivity(new Intent(context, UpdateActivity.class));
	}

	public static void startAboutActivity(Context context) {
		context.startActivity(new Intent(context, AboutActivity.class));
	}

	public static void startTutorialActivity(Context context) {
		context.startActivity(new Intent(context, TutorialActivity.class));
	}

	public static void startLocationSettings(Context context) {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		context.startActivity(intent);
	}

}
