package pl.elabo.hitchhikingmaps.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

/**
 * Created by michalkachel on 22.07.2015.
 */
public class NotificationManager {

	public static void showMessage(@NonNull Context context, @NonNull String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}
