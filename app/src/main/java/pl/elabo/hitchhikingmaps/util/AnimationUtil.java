package pl.elabo.hitchhikingmaps.util;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.view.View;

/**
 * Created by michalkachel on 07.10.2015.
 */
public class AnimationUtil {

	public static void startColorBackgroundAnimation(final View view, int colorFrom, int colorTo, int duration) {
		ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
		colorAnimation.setDuration(duration);
		colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				view.setBackgroundColor((Integer) valueAnimator.getAnimatedValue());
			}
		});
		colorAnimation.start();
	}

}
