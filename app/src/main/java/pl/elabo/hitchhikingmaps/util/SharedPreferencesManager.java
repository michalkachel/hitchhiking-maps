package pl.elabo.hitchhikingmaps.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by michalkachel on 12.07.2015.
 */
public class SharedPreferencesManager {
	public static final String APP_SHARED_PREF_KEY = "HitchHikingApp";

	public static final String KEY_WAS_FIRST_UPDATE = "was_first_update";

	private static SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences(APP_SHARED_PREF_KEY, Context.MODE_PRIVATE);
	}

	public static void setWasFirstUpdate(Context context, boolean wasFirstUpdate) {
		SharedPreferences.Editor editor = getSharedPreferences(context).edit();
		editor.putBoolean(KEY_WAS_FIRST_UPDATE, wasFirstUpdate);
		editor.apply();
	}

	public static boolean wasFirstUpdate(Context context) {
		return getSharedPreferences(context).getBoolean(KEY_WAS_FIRST_UPDATE, false);
	}

}
