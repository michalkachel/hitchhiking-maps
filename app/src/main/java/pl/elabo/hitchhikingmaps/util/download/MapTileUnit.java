package pl.elabo.hitchhikingmaps.util.download;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ELABO on 04.11.2015.
 */
public class MapTileUnit {

	public static final String[] SOURCES = {"a", "b", "c"};
	public static final String TILE_URL = "http://%s.tile.openstreetmap.org/%d/%d/%d.png"; // http://[abc].tile.openstreetmap.org/zoom/x/y.png
	private static Random sRandom = new Random();
	private int mZoomLevel;
	private int mX;
	private int mY;

	private MapTileUnit(int zoomLevel, int x, int y) {
		mZoomLevel = zoomLevel;
		mX = x;
		mY = y;
	}

	public static List<MapTileUnit> createMapTileUnits(MapBox mapBox) {
		List<MapTileUnit> mapTiles = new ArrayList<>();
		final int zoomLevel = mapBox.getZoomLevel();

		int westTileNumber = MapTileHelper.xTileNumber(mapBox.getWest(), zoomLevel);
		int eastTileNumber = MapTileHelper.xTileNumber(mapBox.getEast(), zoomLevel);
		int southTileNumber = MapTileHelper.yTileNumber(mapBox.getSouth(), zoomLevel);
		int northTileNumber = MapTileHelper.yTileNumber(mapBox.getNorth(), zoomLevel);

		int startX = Math.min(westTileNumber, eastTileNumber);
		int endX = Math.max(westTileNumber, eastTileNumber);
		int startY = Math.min(southTileNumber, northTileNumber);
		int endY = Math.max(southTileNumber, northTileNumber);

		int currentX = startX;
		while (currentX <= endX) {
			int currentY = startY;
			while (currentY <= endY) {
				mapTiles.add(new MapTileUnit(zoomLevel, currentX, currentY));
				currentY++;
			}
			currentX++;
		}

		return mapTiles;
	}

	public int getZoomLevel() {
		return mZoomLevel;
	}

	public int getX() {
		return mX;
	}

	public int getY() {
		return mY;
	}

	public String url() {
		String source = SOURCES[sRandom.nextInt(SOURCES.length)];
		return String.format(TILE_URL, source, mZoomLevel, mX, mY);
	}
}
