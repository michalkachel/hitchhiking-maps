package pl.elabo.hitchhikingmaps.util;

import android.content.Context;
import android.location.LocationManager;

import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;

import java.util.List;

import pl.elabo.hitchhikingmaps.model.entity.Place;

/**
 * Created by michalkachel on 26.07.2015.
 */
public class LocationUtil {

	public static boolean isLocationEnabled(Context context) {
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}

	public static GeoPoint placeToGeoPoint(Place place) {
		return new GeoPoint(place.getLatitude(), place.getLongitude());
	}

	public static BoundingBoxE6 getBoundingBox(List<Place> places, boolean addPadding) {
		double minLon = Double.MAX_VALUE;
		double maxLon = Double.MIN_VALUE;
		double minLat = Double.MAX_VALUE;
		double maxLat = Double.MIN_VALUE;

		for (Place place : places) {
			minLon = Math.min(minLon, place.getLongitude());
			maxLon = Math.max(maxLon, place.getLongitude());
			minLat = Math.min(minLat, place.getLatitude());
			maxLat = Math.max(maxLat, place.getLatitude());
		}

		if (addPadding) {
			double latPadding = mapPadding(maxLat, minLat);
			double lonPadding = mapPadding(maxLon, minLon);

			maxLat += latPadding;
			maxLon += lonPadding;
			minLat -= latPadding;
			minLon -= lonPadding;
		}

		return new BoundingBoxE6(maxLat, maxLon, minLat, minLon);
	}

	private static double mapPadding(double max, double min) {
		return Math.abs((max - min) / 5.0);
	}

	/*
 * Calculate distance between two points in latitude and longitude taking
 * into account height difference. If you are not interested in height
 * difference pass 0.0. Uses Haversine method as its base.
 *
 * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
 * el2 End altitude in meters
 * @returns Distance in Meters
 */
	public static double distance(double lat1, double lat2, double lon1,
	                              double lon2, double el1, double el2) {

		final int R = 6371; // Radius of the earth

		Double latDistance = Math.toRadians(lat2 - lat1);
		Double lonDistance = Math.toRadians(lon2 - lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = el1 - el2;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return Math.sqrt(distance);
	}

}
