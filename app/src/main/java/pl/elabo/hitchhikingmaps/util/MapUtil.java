package pl.elabo.hitchhikingmaps.util;

import android.os.Build;
import android.view.ViewTreeObserver;

import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.views.MapView;

/**
 * Created by ELABO on 06.11.15.
 */
public class MapUtil {

	public static void zoomToBoundingBox(final MapView mapView, final BoundingBoxE6 boundingBoxE6) {
		mapView.getController().setCenter(boundingBoxE6.getCenter());
		mapView.getController().setZoom(mapView.getMaxZoomLevel());

		if (mapView.getScreenRect(null).height() > 0) {
			mapView.zoomToBoundingBox(boundingBoxE6);
		} else {
			ViewTreeObserver vto1 = mapView.getViewTreeObserver();
			vto1.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@SuppressWarnings("deprecation")
				@Override
				public void onGlobalLayout() {
					mapView.zoomToBoundingBox(boundingBoxE6);
					ViewTreeObserver vto2 = mapView.getViewTreeObserver();
					if (Build.VERSION.SDK_INT < 16) {
						vto2.removeGlobalOnLayoutListener(this);
					} else {
						vto2.removeOnGlobalLayoutListener(this);
					}
				}
			});
		}
	}

}
