package pl.elabo.hitchhikingmaps.util.download;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ELABO on 04.11.2015.
 */
public class MapBox {
	public static final int BASE_OFFSET_IN_METERS = 300;
	public static final double UNIT_TO_METERS = 0.00001; //// TODO: 04.11.2015 change according to latitude

	private int mZoomLevel;
	private double mNorth;
	private double mSouth;
	private double mEast;
	private double mWest;

	private MapBox(int zoomLevel, double north, double east, double south, double west) {
		mZoomLevel = zoomLevel;
		mNorth = north;
		mEast = east;
		mSouth = south;
		mWest = west;
	}

	public static List<MapBox> createPlaceMapBoxes(int maxZoomLevel, List<Integer> zoomLevels, double latitude, double longitude) {
		List<MapBox> mapBoxes = new ArrayList<>();
		for (int zoomLevel : zoomLevels) {
			if (zoomLevel <= maxZoomLevel) {
				mapBoxes.add(MapBox.createMapBox(maxZoomLevel, zoomLevel, latitude, longitude));
			}
		}
		return mapBoxes;
	}

	private static MapBox createMapBox(int maxDownloadZoom, int zoomLevel, double latitude, double longitude) {
		int offsetInMeters = (int) Math.pow(2, maxDownloadZoom - zoomLevel) * BASE_OFFSET_IN_METERS;
		double offset = (double) offsetInMeters * UNIT_TO_METERS;
		double north = latitude + offset;
		double south = latitude - offset;
		double east = longitude + offset;
		double west = longitude - offset;

		return new MapBox(zoomLevel, north, east, south, west);
	}

	public int getZoomLevel() {
		return mZoomLevel;
	}

	public double getNorth() {
		return mNorth;
	}

	public double getSouth() {
		return mSouth;
	}

	public double getEast() {
		return mEast;
	}

	public double getWest() {
		return mWest;
	}
}
