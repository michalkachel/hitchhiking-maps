package pl.elabo.hitchhikingmaps.util;

import android.os.Environment;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okio.BufferedSink;
import okio.Okio;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.util.download.MapBox;
import pl.elabo.hitchhikingmaps.util.download.MapTileUnit;
import timber.log.Timber;

/**
 * Created by ELABO on 04.11.2015.
 */
public class MapDownloadUtil {
	public static final String MAPNIK_DIR = "/osmdroid/tiles/Mapnik/";
	public static final int MAX_DOWNLOAD_ZOOM_LEVEL = 16;
	public static final Integer[] ZOOM_LEVELS = new Integer[]{16, 15, 14, 13};

	public static void downloadMap(OnProgressListener onProgressListener, Plan plan) throws IOException {
		long startTime = System.currentTimeMillis();
		String fullMapnikPath = Environment.getExternalStorageDirectory().getPath() + MAPNIK_DIR;

		List<MapTileUnit> allMapTileUnits = getAllMapTiles(plan);

		int progressMax = allMapTileUnits.size();
		int progress = 0;

		for (MapTileUnit mapTileUnit : allMapTileUnits) {
			downloadTile(fullMapnikPath, mapTileUnit);
			progress++;
			if (onProgressListener != null) {
				onProgressListener.onProgressChanged(progress, progressMax);
			}
		}

		for (Place place : plan.getPlaces()) {
			place.setMapDownloaded(true);
			place.save();
		}

		Timber.d("Time of download: " + (System.currentTimeMillis() - startTime));

	}

	private static void downloadTile(String dirPath, MapTileUnit mapTileUnit) throws IOException {
		StringBuilder outputPathBuilder = new StringBuilder(dirPath);
		outputPathBuilder.append(mapTileUnit.getZoomLevel());
		outputPathBuilder.append("/");
		outputPathBuilder.append(mapTileUnit.getX());
		outputPathBuilder.append("/");

		File outputDir = new File(outputPathBuilder.toString());
		if (!outputDir.exists()) {
			outputDir.mkdirs();
		}

		String tileFileName = mapTileUnit.getY() + ".png.tile";
		File downloadedTile = new File(outputDir, tileFileName);

		if (!downloadedTile.exists()) {
			OkHttpClient client = new OkHttpClient();
			Request request = new Request.Builder().url(mapTileUnit.url()).build();
			Response response = client.newCall(request).execute();

			BufferedSink sink = Okio.buffer(Okio.sink(downloadedTile));
			sink.writeAll(response.body().source());
			sink.close();
		}
	}

	private static List<MapTileUnit> getAllMapTiles(Plan plan) {
		List<MapTileUnit> allMapTileUnits = new ArrayList<>();
		for (Place place : extractPlacesWithoutMap(plan)) {
			allMapTileUnits.addAll(getPlaceMapTiles(place));
		}
		Timber.d("All tiles size: " + allMapTileUnits.size());
		return allMapTileUnits;
	}

	private static List<Place> extractPlacesWithoutMap(Plan plan) {
		List<Place> placesToDownloadMap = new ArrayList<>();
		for (Place place : plan.getPlaces()) {
			if (!place.isMapDownloaded()) {
				placesToDownloadMap.add(place);
			}
		}
		return placesToDownloadMap;
	}

	private static List<MapTileUnit> getPlaceMapTiles(Place place) {
		List<MapTileUnit> placeMapTileUnits = new ArrayList<>();
		for (MapBox mapBox : MapBox.createPlaceMapBoxes(MAX_DOWNLOAD_ZOOM_LEVEL, getZoomLevels(), place.getLatitude(), place.getLongitude())) {
			placeMapTileUnits.addAll(MapTileUnit.createMapTileUnits(mapBox));
		}
		return placeMapTileUnits;
	}

	private static List<Integer> getZoomLevels() {
		return Arrays.asList(ZOOM_LEVELS);
	}

	public interface OnProgressListener {
		void onProgressChanged(int progress, int max);
	}

}
