package pl.elabo.hitchhikingmaps.util.download;

/**
 * Created by ELABO on 04.11.2015.
 */
public class MapTileHelper {

	public static int xTileNumber(final double longitude, final int zoom) {
		int xTileNumber = (int) Math.floor((longitude + 180) / 360 * (1 << zoom));
		if (xTileNumber < 0) {
			xTileNumber = 0;
		} else if (xTileNumber >= (1 << zoom)) {
			xTileNumber = ((1 << zoom) - 1);
		}
		return xTileNumber;
	}

	public static int yTileNumber(final double latitude, final int zoom) {
		int yTileNumber = (int) Math.floor((1 - Math.log(Math.tan(Math.toRadians(latitude)) + 1 / Math.cos(Math.toRadians(latitude))) / Math.PI) / 2 * (1 << zoom));
		if (yTileNumber < 0) {
			yTileNumber = 0;
		} else if (yTileNumber >= (1 << zoom)) {
			yTileNumber = ((1 << zoom) - 1);
		}
		return yTileNumber;
	}

}
