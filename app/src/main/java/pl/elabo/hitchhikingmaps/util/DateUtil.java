package pl.elabo.hitchhikingmaps.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ELABO on 06.11.15.
 */
public class DateUtil {

	public static final DateFormat mShowDateFormat = new SimpleDateFormat("dd MMMM y");

	public static String formatDate(Date date) {
		return mShowDateFormat.format(date);
	}

}
