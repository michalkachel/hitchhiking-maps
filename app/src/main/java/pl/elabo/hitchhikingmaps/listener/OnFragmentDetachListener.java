package pl.elabo.hitchhikingmaps.listener;

/**
 * Created by ELABO on 14.11.15.
 */
public interface OnFragmentDetachListener {
	void onFragmentDetached();
}
