package pl.elabo.hitchhikingmaps.listener;

/**
 * Created by michalkachel on 15.08.2015.
 */
public interface OnPlaceSelectionListener {

	void onPlaceSelected(long remoteId);

}
