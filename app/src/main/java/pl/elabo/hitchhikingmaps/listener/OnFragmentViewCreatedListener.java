package pl.elabo.hitchhikingmaps.listener;

import android.widget.ScrollView;

/**
 * Created by michalkachel on 15.08.2015.
 */
public interface OnFragmentViewCreatedListener {

	void onScrollViewCreated(ScrollView view);

}
