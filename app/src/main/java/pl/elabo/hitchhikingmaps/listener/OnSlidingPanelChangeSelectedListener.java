package pl.elabo.hitchhikingmaps.listener;

/**
 * Created by michalkachel on 15.08.2015.
 */
public interface OnSlidingPanelChangeSelectedListener {

	void onPanelToggleStateSelected();

	void onPanelTouchEnabled(boolean enabled);

}