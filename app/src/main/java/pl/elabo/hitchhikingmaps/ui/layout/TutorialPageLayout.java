package pl.elabo.hitchhikingmaps.ui.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.elabo.hitchhikingmaps.R;

/**
 * Created by michalkachel on 17.08.2015.
 */
public class TutorialPageLayout extends LinearLayout {

	@Bind(R.id.tutorial_image)
	ImageView mTutorialImage;

	@Bind(R.id.tutorial_text)
	TextView mTutorialText;

	public TutorialPageLayout(Context context) {
		super(context);
	}

	public TutorialPageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TutorialPageLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public TutorialPageLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		ButterKnife.bind(this);
	}

	public void setTutorialPage(@DrawableRes int tutorialImageId, @StringRes int tutorialTextId) {
		mTutorialImage.setImageResource(tutorialImageId);
		mTutorialText.setText(Html.fromHtml(getContext().getString(tutorialTextId)));
	}
}
