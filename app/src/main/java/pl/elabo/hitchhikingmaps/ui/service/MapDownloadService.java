package pl.elabo.hitchhikingmaps.ui.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;

import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.application.Navigator;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.util.MapDownloadUtil;

/**
 * Created by ELABO on 05.11.2015.
 */
public class MapDownloadService extends IntentService implements MapDownloadUtil.OnProgressListener {
	public static final String TAG = "map_download_service";
	public static final int NOTIFICATION_ID_BASE = 9466;
	private Plan mPlan;
	private int mNotificationId;
	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mNotificationBuilder;
	public MapDownloadService() {
		super(TAG);
	}

	public static void startDownloading(Context context, long planId) {
		Intent serviceIntent = new Intent(context, MapDownloadService.class);
		serviceIntent.putExtra(AppConstants.Extras.PLAN_ID, planId);
		context.startService(serviceIntent);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		boolean success = false;
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		long planId = intent.getExtras().getLong(AppConstants.Extras.PLAN_ID);
		mPlan = Plan.load(Plan.class, planId);
		mNotificationId = (int) (NOTIFICATION_ID_BASE + mPlan.getId());

		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, Navigator.getPlanMapIntent(this, mPlan.getId(), true), PendingIntent.FLAG_UPDATE_CURRENT);

		mNotificationBuilder = new NotificationCompat.Builder(this)
				.setProgress(100, 0, false)
				.setContentIntent(pendingIntent)
				.setAutoCancel(false)
				.setOngoing(true)
				.setSmallIcon(R.drawable.ic_notification)
				.setTicker(getString(R.string.message_downloading_map))
				.setContentTitle(getString(R.string.message_downloading_plan_map) + mPlan.getName());

		mNotificationManager.notify(mNotificationId, mNotificationBuilder.build());

		try {
			MapDownloadUtil.downloadMap(this, mPlan);
			success = true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (success) {
			mNotificationBuilder.setSubText(getString(R.string.message_map_downloaded_successfully));
		} else {
			mNotificationBuilder.setSubText(getString(R.string.message_error_occured));
		}
		mNotificationBuilder.setOngoing(false);
		mNotificationBuilder.setAutoCancel(true);
		mNotificationManager.notify(mNotificationId, mNotificationBuilder.build());
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		super.onTaskRemoved(rootIntent);
		mNotificationManager.cancel(mNotificationId);
	}

	@Override
	public void onProgressChanged(int progress, int max) {
		mNotificationBuilder.setProgress(max, progress, false);
		mNotificationBuilder.setContentText(progress + "/" + max);
		mNotificationManager.notify(mNotificationId, mNotificationBuilder.build());
	}
}
