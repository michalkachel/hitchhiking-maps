package pl.elabo.hitchhikingmaps.ui.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.model.entity.PlaceDescription;

/**
 * Created by michalkachel on 17.08.2015.
 */
public class DescriptionLayout extends LinearLayout {

	@Bind(R.id.description)
	TextView mDescription;

	@Bind(R.id.author)
	TextView mAuthor;

	@Bind(R.id.divider)
	View mDivider;

	public DescriptionLayout(Context context) {
		super(context);
	}

	public DescriptionLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DescriptionLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public DescriptionLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		ButterKnife.bind(this);
	}

	public void setDescription(PlaceDescription placeDescription, boolean isLast) {
		mDescription.setText(placeDescription.getDescription());
		mAuthor.setVisibility(View.GONE);
		if (isLast) {
			mDivider.setVisibility(View.GONE);
		}
	}
}
