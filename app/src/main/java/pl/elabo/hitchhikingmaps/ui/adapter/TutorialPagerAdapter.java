package pl.elabo.hitchhikingmaps.ui.adapter;

import android.support.v4.view.PagerAdapter;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.ui.layout.TutorialPageLayout;

/**
 * Created by ELABO on 13.11.15.
 */
public class TutorialPagerAdapter extends PagerAdapter {

	LayoutInflater mLayoutInflater;
	List<Pair<Integer, Integer>> mPagePairs;

	public TutorialPagerAdapter(LayoutInflater layoutInflater, List<Pair<Integer, Integer>> pagePairs) {
		mLayoutInflater = layoutInflater;
		mPagePairs = pagePairs;
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {
		Pair<Integer, Integer> pagePair = mPagePairs.get(position);
		TutorialPageLayout tutorialPage = (TutorialPageLayout) mLayoutInflater.inflate(R.layout.layout_tutorial_page, null);
		tutorialPage.setTutorialPage(pagePair.first, pagePair.second);

		container.addView(tutorialPage);

		return tutorialPage;
	}

	@Override
	public int getCount() {
		return mPagePairs.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == object);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}
}
