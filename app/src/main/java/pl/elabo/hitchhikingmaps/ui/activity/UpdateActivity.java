package pl.elabo.hitchhikingmaps.ui.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.OnClick;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.mvp.presenter.UpdatePresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.UpdatePresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.UpdateView;
import pl.elabo.hitchhikingmaps.ui.activity.base.BaseActivity;
import pl.elabo.hitchhikingmaps.util.NetworkUtil;
import pl.elabo.hitchhikingmaps.util.NotificationManager;


public class UpdateActivity extends BaseActivity implements UpdateView {

	protected ProgressDialog mProgressDialog;
	@Bind(R.id.added_places)
	protected TextView mPlacesAdded;
	@Bind(R.id.update_button)
	protected Button mUpdateButton;
	private UpdatePresenter mUpdatePresenter;

	@Override
	protected int getLayoutId() {
		return R.layout.activity_update;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (mUpdatePresenter == null) {
			mUpdatePresenter = new UpdatePresenterImpl(this);
		}

		mUpdatePresenter.onCreate();
	}

	@Override
	protected boolean hasDrawerToggle() {
		return false;
	}

	@Override
	public void showError(Throwable throwable) {
		NotificationManager.showMessage(UpdateActivity.this, throwable.getMessage());
	}

	@Override
	public void setScreenTitle() {
		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setTitle(R.string.title_update);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}
	}

	@Override
	public void showUpdateDialog() {
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialogInterface) {
				mUpdatePresenter.onUpdateInterrupted();
			}
		});
		mProgressDialog.setTitle(getString(R.string.title_updating_spots));
		mProgressDialog.setMessage(getString(R.string.message_please_wait_update));
		mProgressDialog.show();
	}

	@Override
	public void dismissUpdateDialog() {
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
	}

	@Override
	protected void setNavigationItemSelected() {
		mNavigationView.setCheckedItem(R.id.drawer_update);
	}

	@Override
	public void notifyPlacesAdded(int addedPlacesCounter) {
		mPlacesAdded.setText(Html.fromHtml(String.format(getString(R.string.message_added_new_places), addedPlacesCounter)));
	}

	@Override
	public void disableUpdate() {
		mUpdateButton.setEnabled(false);
	}

	@Override
	public boolean isNetworkEnabled() {
		return NetworkUtil.isNetworkEnabled(this);
	}

	@Override
	public void notifyInternetConnectionNotAvailable() {
		NotificationManager.showMessage(this, getString(R.string.message_internet_not_available));
	}

	@OnClick(R.id.update_button)
	void onUpdate() {
		mUpdatePresenter.onUpdate();
	}
}
