package pl.elabo.hitchhikingmaps.ui.fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import butterknife.Bind;
import butterknife.OnClick;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.mvp.presenter.MainPlaceDetailPresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.MainPlaceDetailPresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.MainPlaceDetailView;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BasePlaceDetailFragment;

/**
 * Created by michalkachel on 22.07.2015.
 */
public class MainPlaceDetailFragment extends BasePlaceDetailFragment<MainPlaceDetailPresenter> implements MainPlaceDetailView {
	public static final String FRAGMENT_TAG = MainPlaceDetailFragment.class.getName();

	@Bind(R.id.add_to_plan)
	FloatingActionButton mAddToPlanButton;

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_main_place_detail;
	}

	@Override
	protected MainPlaceDetailPresenter getPlaceDetailPresenter() {
		return new MainPlaceDetailPresenterImpl(this);
	}

	@OnClick(R.id.add_to_plan)
	void onAddToPlan() {
		mPlaceDetailPresenter.onAddToPlan();
	}

	@Override
	public void showAddToPlanDialog(long id) {
		AddToPlanDialogFragment addToPlanDialogFragment = new AddToPlanDialogFragment();
		Bundle args = new Bundle();
		args.putLong(AppConstants.Extras.PLACE_REMOTE_ID, id);
		addToPlanDialogFragment.setArguments(args);
		addToPlanDialogFragment.show(getChildFragmentManager(), AddToPlanDialogFragment.FRAGMENT_TAG);
	}

	@Override
	public void showPlaceDetailHolder() {
		super.showPlaceDetailHolder();
		mAddToPlanButton.setVisibility(View.GONE);
		mAddToPlanButton.show();
	}

}
