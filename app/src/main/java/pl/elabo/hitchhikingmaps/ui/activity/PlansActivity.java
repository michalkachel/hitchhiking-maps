package pl.elabo.hitchhikingmaps.ui.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.ui.activity.base.BaseActivity;
import pl.elabo.hitchhikingmaps.ui.fragment.PlansListFragment;

/**
 * Created by michalkachel on 12.10.2015.
 */
public class PlansActivity extends BaseActivity {

	@Override
	protected int getLayoutId() {
		return R.layout.activity_plans;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setTitle(R.string.title_my_plans);
		}

		getSupportFragmentManager().beginTransaction().replace(R.id.container, new PlansListFragment()).commit();

	}

	@Override
	protected void setNavigationItemSelected() {
		mNavigationView.setCheckedItem(R.id.drawer_plans);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.menu_plans, menu);
		return true;
	}
}
