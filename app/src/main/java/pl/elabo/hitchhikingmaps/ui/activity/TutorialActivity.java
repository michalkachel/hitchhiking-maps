package pl.elabo.hitchhikingmaps.ui.activity;

import android.os.Bundle;

import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.ui.activity.base.BaseActivity;
import pl.elabo.hitchhikingmaps.ui.fragment.TutorialFragment;

/**
 * Created by ELABO on 12.11.15.
 */
public class TutorialActivity extends BaseActivity {

	@Override
	protected int getLayoutId() {
		return R.layout.activity_tutorial;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setTitle(R.string.title_tutorial);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}

		getSupportFragmentManager().beginTransaction().add(R.id.tutorial_container, new TutorialFragment(), TutorialFragment.FRAGMENT_TAG).commit();
	}

	@Override
	protected boolean hasDrawerToggle() {
		return false;
	}

	@Override
	protected void setNavigationItemSelected() {
		mNavigationView.setCheckedItem(R.id.drawer_tutorial);
	}
}
