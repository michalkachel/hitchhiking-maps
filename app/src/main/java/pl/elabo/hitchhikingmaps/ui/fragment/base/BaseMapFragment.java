package pl.elabo.hitchhikingmaps.ui.fragment.base;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.app.AlertDialog;
import android.view.View;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.application.Navigator;
import pl.elabo.hitchhikingmaps.listener.OnPlaceSelectionListener;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.MapPresenter;
import pl.elabo.hitchhikingmaps.mvp.view.base.BaseMapView;
import pl.elabo.hitchhikingmaps.util.LocationUtil;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 22.07.2015.
 */
public abstract class BaseMapFragment<T extends MapPresenter> extends BaseFragment implements BaseMapView,
		ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {

	public static final String FRAGMENT_TAG = BaseMapFragment.class.getName();
	protected final int mAllItemsOverlayIndex = 1;
	protected final int mLocationOverlayIndex = 0;
	protected final int mMarkedItemOverlayIndex = 2;

	protected T mMapPresenter;
	protected OnPlaceSelectionListener mOnPlaceSelectionListener;

	protected MyLocationNewOverlay mLocationOverlay;

	@Bind(R.id.map)
	protected MapView mMapView;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		mOnPlaceSelectionListener = (OnPlaceSelectionListener) context;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mMapPresenter = getMapPresenter();
	}

	@Override
	public void onResume() {
		super.onResume();
		mMapPresenter.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		mMapPresenter.onPause();
	}

	protected abstract T getMapPresenter();

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mMapPresenter.onViewCreated();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mMapPresenter.onDestroyView();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mOnPlaceSelectionListener = null;
	}

	@UiThread
	private void populateMapWith(ItemizedIconOverlay<OverlayItem> itemizedIconOverlay) {
		if (mMapView.getOverlays().size() > 1) {
			mMapView.getOverlayManager().remove(mAllItemsOverlayIndex);
		}
		mMapView.getOverlayManager().add(mAllItemsOverlayIndex, itemizedIconOverlay);
		mMapView.invalidate();
	}

	@Override
	public void initializeMap() {
		mMapView.setTilesScaledToDpi(true);
		mMapView.setMultiTouchControls(true);
		mMapView.setMinZoomLevel(getMapMinZoomLevel());
		setInitialMapPosition(mMapView);
	}

	@Override
	public void initializeMyLocation() {
		mLocationOverlay = new MyLocationNewOverlay(getContext(), mMapView);
		mLocationOverlay.setDrawAccuracyEnabled(true);
		mMapView.getOverlayManager().add(mLocationOverlayIndex, mLocationOverlay);
	}

	protected int getMapMinZoomLevel() {
		return AppConstants.Map.GLOBAL_MIN_ZOOM_LEVEL;
	}

	protected void setInitialMapPosition(MapView mapView) {
		mapView.getController().setCenter(new GeoPoint(AppConstants.Map.GLOBAL_INITIAL_LATITUDE, AppConstants.Map.GLOBAL_INITIAL_LONGITUDE));
		mapView.getController().setZoom(AppConstants.Map.GLOBAL_INITIAL_ZOOM_LEVEL);
	}

	@Override
	public void showPlaces(List<Place> places) {
		if (isAdded()){
			Drawable mapPinGreen = getResources().getDrawable(R.drawable.map_spot_green);
			Drawable mapPinYellow = getResources().getDrawable(R.drawable.map_spot_yellow);
			Drawable mapPinRed = getResources().getDrawable(R.drawable.map_spot_red);
			Drawable mapPinWhite = getResources().getDrawable(R.drawable.map_spot_white);
			final ItemizedIconOverlay<OverlayItem> iconsOverlay = new ItemizedIconOverlay<>(new ArrayList<OverlayItem>(), mapPinGreen, this, new DefaultResourceProxyImpl(getActivity()));

			for (Place place : places) {
				OverlayItem overlayItem = new OverlayItem(String.valueOf(place.getRemoteId()), String.valueOf(place.getRating()), new GeoPoint(place.getLatitude(), place.getLongitude()));
				switch (place.getRating()) {
					case 0:
						overlayItem.setMarker(mapPinWhite);
						break;
					case 5:
						overlayItem.setMarker(mapPinRed);
						break;
					case 4:
					case 3:
						overlayItem.setMarker(mapPinYellow);
						break;
				}
				iconsOverlay.addItem(overlayItem);
			}

			if (getActivity() != null) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (mMapView != null) {
							populateMapWith(iconsOverlay);
						}
					}
				});
			}
		}
	}

	@Override
	public void markPointOnMap(final IGeoPoint geoPoint, final boolean doAnimateToPoint) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mMapView.getOverlays().size() > mMarkedItemOverlayIndex) {
					mMapView.getOverlayManager().remove(mMarkedItemOverlayIndex);
				}

				mMapView.getOverlays().add(mMarkedItemOverlayIndex, getMarkedPointOverlay(geoPoint));

				mMapView.invalidate();
				if (doAnimateToPoint) {
					mMapView.getController().animateTo(geoPoint);
				}
			}
		});
	}

	@Override
	public boolean isLocationEnabled() {
		return LocationUtil.isLocationEnabled(getContext());
	}

	@Override
	public boolean isLocationDetected() {
		return mLocationOverlay.getLastFix() != null;
	}

	@Override
	public void enableMyLocation() {
		if (mLocationOverlay != null) {
			mLocationOverlay.enableMyLocation();
		}
	}

	@Override
	public void followMyLocation() {
		if (mLocationOverlay != null) {
			mLocationOverlay.enableFollowLocation();
		}
	}

	@Override
	public void disableMyLocation() {
		if (mLocationOverlay != null) {
			mLocationOverlay.disableMyLocation();
		}
	}

	@Override
	public void showNoLocationDetected() {
		NotificationManager.showMessage(getContext(), getString(R.string.message_no_location_detected));
	}

	@Override
	public void askForLocationEnabling() {
		new AlertDialog.Builder(getActivity())
				.setTitle(getString(R.string.title_location_not_enabled))
				.setMessage(getString(R.string.question_enable_location))
				.setPositiveButton(getActivity().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mMapPresenter.onEnableLocation();
						dialog.dismiss();
					}
				})
				.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.setCancelable(true)
				.show();
	}

	@Override
	public void navigateToLocationSettings() {
		Navigator.startLocationSettings(getContext());
	}

	private ItemizedIconOverlay<OverlayItem> getMarkedPointOverlay(IGeoPoint geoPoint) {
		Drawable mapPinFocused = getResources().getDrawable(R.drawable.map_spot_focused);
		final ItemizedIconOverlay<OverlayItem> markedPointOverlay = new ItemizedIconOverlay<>(new ArrayList<OverlayItem>(), mapPinFocused, this, new DefaultResourceProxyImpl(getActivity()));
		markedPointOverlay.addItem(new OverlayItem(null, null, geoPoint));
		return markedPointOverlay;
	}

	@Override
	public void showPlaceDetails(final long remoteId) {
		if (mOnPlaceSelectionListener != null) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mOnPlaceSelectionListener.onPlaceSelected(remoteId);
				}
			});
		}
	}

	@Override
	public void showError(final Throwable throwable) {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					NotificationManager.showMessage(getActivity(), throwable.getMessage());
				}
			});
		}
	}

	@OnClick(R.id.show_my_location)
	void onShowMyLocation() {
		mMapPresenter.onShowMyLocation();
	}

	@Override
	public boolean onItemSingleTapUp(int index, OverlayItem item) {
		if (item.getTitle() != null) {
			mMapPresenter.onPlaceSelected(Long.parseLong(item.getTitle()), item.getPoint());
		}
		return true;
	}

	@Override
	public boolean onItemLongPress(int index, OverlayItem item) {
		return false;
	}
}
