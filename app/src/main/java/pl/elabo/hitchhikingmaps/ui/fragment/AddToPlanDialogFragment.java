package pl.elabo.hitchhikingmaps.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.mvp.presenter.AddToPlanPresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.AddToPlanPresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.AddToPlanView;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BaseDialogFragment;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 20.08.2015.
 */
public class AddToPlanDialogFragment extends BaseDialogFragment implements AddToPlanView {
	public static final String FRAGMENT_TAG = AddToPlanDialogFragment.class.getName();
	@Bind(R.id.create_new_plan_holder)
	ViewGroup mCreateNewPlanHolder;
	@Bind(R.id.plans_spinner_holder)
	ViewGroup mPlansSpinnerHolder;
	@Bind(R.id.plans_spinner)
	Spinner mPlansSpinner;
	@Bind(R.id.message)
	TextView mMessage;
	@Bind(R.id.plan_name)
	EditText mPlanName;
	@Bind(R.id.create_new_plan)
	Button mCreateNewPlan;
	@Bind(R.id.add_to_plan)
	Button mAddToPlan;
	private AddToPlanPresenter mAddToPlanPresenter;
	private long mPlaceRemoteId;

	@Override
	protected int getLayoutId() {
		return R.layout.dialog_add_to_plan;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mPlaceRemoteId = getArguments().getLong(AppConstants.Extras.PLACE_REMOTE_ID);

		if (mAddToPlanPresenter == null) {
			mAddToPlanPresenter = new AddToPlanPresenterImpl(this, mPlaceRemoteId);
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mAddToPlanPresenter.onViewCreated();

		getDialog().setTitle(R.string.title_choose_plan);
	}

	@OnClick(R.id.create_new_plan)
	void onCreateNewPlan() {
		mAddToPlanPresenter.onCreatePlan(mPlanName.getText().toString());
	}

	@OnClick(R.id.new_plan)
	void onNewPlan() {
		mAddToPlanPresenter.onNewPlan();
	}

	@OnClick(R.id.cancel)
	void onCancel() {
		mAddToPlanPresenter.onCancel();
	}

	@OnClick(R.id.add_to_plan)
	void onAddToPlan() {
		mAddToPlanPresenter.onAddToPlan(((Plan) mPlansSpinner.getAdapter().getItem(mPlansSpinner.getSelectedItemPosition())).getId());
	}

	@OnTextChanged(R.id.plan_name)
	void onPlanNameChanged(CharSequence text) {
		mAddToPlanPresenter.onPlanNameTextChanged(text.toString());
	}

	@OnFocusChange(R.id.plan_name)
	void onFocusChanged(boolean focused) {
		InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		if (focused) {
			inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
		} else {
			inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		}
	}

	@Override
	public void dismissDialog() {
		dismiss();
	}

	@Override
	public void setNoPlansAvailableVisibility(boolean visible) {
		mMessage.setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	@Override
	public void setCreateNewPlanHolderVisibility(boolean visible) {
		mCreateNewPlanHolder.setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	@Override
	public void setPlansHolderVisibility(boolean visible) {
		mPlansSpinnerHolder.setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	@Override
	public void showPlans(List<Plan> plans) {
		ArrayAdapter<Plan> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, plans);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mPlansSpinner.setAdapter(spinnerArrayAdapter);
	}

	@Override
	public void setCreateNewPlanEnabled(boolean enabled) {
		mCreateNewPlan.setEnabled(enabled);
	}

	@Override
	public void setAddToPlanEnabled(boolean enabled) {
		mAddToPlan.setEnabled(enabled);
	}

	@Override
	public void requestFocusAndCleanNewPlanName() {
		mPlanName.requestFocus();
		mPlanName.getText().clear();
	}

	@Override
	public void showPlaceAddedToPlanMessage(String planName) {
		NotificationManager.showMessage(getActivity(), getString(R.string.message_place_added_to_plan) + planName);
	}

	@Override
	public void notifyPlaceAlreadyInPlan(String planName) {
		NotificationManager.showMessage(getActivity(), getString(R.string.message_spot_already_in_plan) + planName);
	}

	@Override
	public void showError(Throwable throwable) {
		NotificationManager.showMessage(getActivity(), throwable.getMessage());
	}
}
