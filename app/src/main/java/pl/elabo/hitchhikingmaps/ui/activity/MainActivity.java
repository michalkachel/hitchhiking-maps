package pl.elabo.hitchhikingmaps.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import butterknife.Bind;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.listener.OnFragmentViewCreatedListener;
import pl.elabo.hitchhikingmaps.listener.OnPlaceSelectionListener;
import pl.elabo.hitchhikingmaps.listener.OnSlidingPanelChangeSelectedListener;
import pl.elabo.hitchhikingmaps.mvp.presenter.MainPresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.MainPresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.MainView;
import pl.elabo.hitchhikingmaps.ui.activity.base.BaseActivity;
import pl.elabo.hitchhikingmaps.ui.fragment.GlobalMapFragment;
import pl.elabo.hitchhikingmaps.ui.fragment.MainPlaceDetailFragment;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BaseMapFragment;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BasePlaceDetailFragment;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 22.07.2015.
 */
public class MainActivity extends BaseActivity implements MainView,
		OnPlaceSelectionListener,
		OnFragmentViewCreatedListener,
		OnSlidingPanelChangeSelectedListener {

	protected BaseMapFragment mMapFragment;
	protected BasePlaceDetailFragment mPlaceDetailFragment;
	@Bind(R.id.sliding_layout)
	SlidingUpPanelLayout mSlidingUpPanelLayout;
	private MainPresenter mMainPresenter;

	@Override
	protected int getLayoutId() {
		return R.layout.activity_main;
	}

	@Override
	protected boolean isRootActivity() {
		return true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (mMainPresenter == null) {
			mMainPresenter = new MainPresenterImpl(this);
		}

		mMainPresenter.onCreate();

	}

	@Override
	protected void setNavigationItemSelected() {
		mNavigationView.setCheckedItem(R.id.drawer_map);
	}

	@Override
	public void onBackPressed() {
		if (isPanelExpanded()) {
			collapseSlidingUpPanel();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void initializeSlidingUpPanel() {
		mSlidingUpPanelLayout.setOverlayed(true);
		mSlidingUpPanelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
			@Override
			public void onPanelSlide(View view, float v) {

			}

			@Override
			public void onPanelCollapsed(View view) {
				mMainPresenter.onPanelStateChanged(false);
			}

			@Override
			public void onPanelExpanded(View view) {
				mMainPresenter.onPanelStateChanged(true);
			}

			@Override
			public void onPanelAnchored(View view) {

			}

			@Override
			public void onPanelHidden(View view) {

			}
		});
	}

	@Override
	public void initializeMap() {
		mMapFragment = new GlobalMapFragment();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.map_container, mMapFragment, GlobalMapFragment.FRAGMENT_TAG)
				.commit();
	}

	@Override
	public void initializePlaceDetail() {
		mPlaceDetailFragment = new MainPlaceDetailFragment();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.place_detail_container, mPlaceDetailFragment, MainPlaceDetailFragment.FRAGMENT_TAG)
				.commit();
	}

	@Override
	public void expandSlidingUpPanel() {
		mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
	}

	@Override
	public void collapseSlidingUpPanel() {
		mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
	}

	@Override
	public void setSlidingUpPanelTouchEnabled(boolean enabled) {
		mSlidingUpPanelLayout.setTouchEnabled(enabled);
	}

	@Override
	public void showPlaceDetail(long remoteId) {
		mPlaceDetailFragment.onPlaceSelected(remoteId);
	}

	@Override
	public void notifyPanelStateChanged(boolean isExpanded) {
		mPlaceDetailFragment.onPanelStateChanged(isExpanded);
	}

	@Override
	public void showError(Throwable throwable) {
		NotificationManager.showMessage(this, throwable.getMessage());
	}

	@Override
	public void onScrollViewCreated(ScrollView scrollView) {
		mSlidingUpPanelLayout.setScrollableView(scrollView);
	}

	@Override
	public void onPanelToggleStateSelected() {
		mMainPresenter.onExpandCollapsePanelSelected(isPanelExpanded());
	}

	@Override
	public void onPanelTouchEnabled(boolean enabled) {
		mMainPresenter.onPanelTouchEnabled(enabled);
	}

	@Override
	public boolean isPanelExpanded() {
		return mSlidingUpPanelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED);
	}

	@Override
	public void onPlaceSelected(long remoteId) {
		mMainPresenter.onPlaceSelected(remoteId);
	}
}
