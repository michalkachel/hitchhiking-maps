package pl.elabo.hitchhikingmaps.ui.fragment.base;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.Navigator;
import pl.elabo.hitchhikingmaps.listener.OnFragmentViewCreatedListener;
import pl.elabo.hitchhikingmaps.listener.OnSlidingPanelChangeSelectedListener;
import pl.elabo.hitchhikingmaps.model.entity.Comment;
import pl.elabo.hitchhikingmaps.model.entity.PlaceDescription;
import pl.elabo.hitchhikingmaps.model.entity.WaitingStatistic;
import pl.elabo.hitchhikingmaps.mvp.presenter.base.PlaceDetailPresenter;
import pl.elabo.hitchhikingmaps.mvp.view.base.BasePlaceDetailView;
import pl.elabo.hitchhikingmaps.ui.layout.CommentLayout;
import pl.elabo.hitchhikingmaps.ui.layout.DescriptionLayout;
import pl.elabo.hitchhikingmaps.ui.layout.RatingStarLayout;
import pl.elabo.hitchhikingmaps.util.AnimationUtil;
import pl.elabo.hitchhikingmaps.util.NetworkUtil;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 22.07.2015.
 */
public abstract class BasePlaceDetailFragment<T extends PlaceDetailPresenter> extends BaseFragment implements BasePlaceDetailView {
	protected T mPlaceDetailPresenter;

	protected OnFragmentViewCreatedListener mOnFragmentViewCreatedListener;
	protected OnSlidingPanelChangeSelectedListener mOnSlidingPanelChangeSelectedListener;

	protected LayoutInflater mLayoutInflater;

	@Bind(R.id.show_street_view)
	FloatingActionButton mShowStreetViewButton;

	@Bind(R.id.select_spot_text)
	TextView mSelectSpotText;

	@Bind(R.id.scroll_view)
	ScrollView mScrollView;

	@Bind(R.id.progress)
	ProgressBar mProgressBar;

	@Bind(R.id.swipe_panel_holder)
	View mSwipePanelHolder;

	@Bind(R.id.expand_collapse_panel)
	Button mExpandCollapseButton;

	@Bind(R.id.rating_star_holder)
	RatingStarLayout mRatingStarHolder;

	@Bind(R.id.location_locality)
	TextView mLocationLocality;

	@Bind(R.id.location_country_continent)
	TextView mLocationCountryContinent;

	@Bind(R.id.rating_count)
	TextView mRatingCount;

	@Bind(R.id.comments_count)
	TextView mCommentsCount;

	@Bind(R.id.waiting_average)
	TextView mWaitingAverage;

	@Bind(R.id.descriptions_holder)
	LinearLayout mDescriptionsHolder;

	@Bind(R.id.descriptions_holder_divider)
	View mDescriptionsHolderDivider;

	@Bind(R.id.comments_holder)
	LinearLayout mCommentsHolder;

	@Bind(R.id.comments_holder_divider)
	View mCommentsHolderDivider;

	@Bind(R.id.waiting_stats)
	TextView mWaitingStats;

	protected abstract T getPlaceDetailPresenter();

	public void onPlaceSelected(long remoteId) {
		mPlaceDetailPresenter.onPlaceSelected(remoteId);
	}

	public void onPanelStateChanged(boolean isExpanded) {
		mPlaceDetailPresenter.onPanelStateChanged(isExpanded);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		mOnFragmentViewCreatedListener = (OnFragmentViewCreatedListener) getActivity();
		mOnSlidingPanelChangeSelectedListener = (OnSlidingPanelChangeSelectedListener) getActivity();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPlaceDetailPresenter = getPlaceDetailPresenter();

		mLayoutInflater = getActivity().getLayoutInflater();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (mOnFragmentViewCreatedListener != null) {
			mOnFragmentViewCreatedListener.onScrollViewCreated(mScrollView);
		}

		mPlaceDetailPresenter.onViewCreated();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mOnFragmentViewCreatedListener = null;
		mOnSlidingPanelChangeSelectedListener = null;
	}

	@Override
	public void showError(final Throwable throwable) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				NotificationManager.showMessage(getActivity(), throwable.getMessage());
			}
		});
	}

	@OnClick(R.id.expand_collapse_panel)
	void onExpandCollapsePanel() {
		mPlaceDetailPresenter.onTogglePanelSelected();
	}

	@OnClick(R.id.show_street_view)
	void onShowStreetView() {
		mPlaceDetailPresenter.onShowStreetView();
	}

	@Override
	public void togglePanelState() {
		if (mOnSlidingPanelChangeSelectedListener != null) {
			mOnSlidingPanelChangeSelectedListener.onPanelToggleStateSelected();
		}
	}

	@Override
	public void showPlaceDetailHolder() {
		mSelectSpotText.setVisibility(View.GONE);
		mScrollView.setVisibility(View.VISIBLE);
		Integer colorFrom = getResources().getColor(R.color.primary_dark_alpha);
		Integer colorTo = getResources().getColor(R.color.primary_dark);
		AnimationUtil.startColorBackgroundAnimation(mSwipePanelHolder, colorFrom, colorTo, 500);

		mShowStreetViewButton.setVisibility(View.GONE);
		mShowStreetViewButton.show();
	}

	@Override
	public void hidePlaceDetailHolder() {
		mSelectSpotText.setVisibility(View.VISIBLE);
		mScrollView.setVisibility(View.GONE);
		mShowStreetViewButton.setVisibility(View.GONE);
	}

	@Override
	public void showProgress() {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mProgressBar.setVisibility(View.VISIBLE);
			}
		});
	}

	@Override
	public void hideProgress() {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mProgressBar.setVisibility(View.GONE);
			}
		});
	}

	@Override
	public void setPanelTouchEnabled(boolean enabled) {
		if (mOnSlidingPanelChangeSelectedListener != null) {
			mOnSlidingPanelChangeSelectedListener.onPanelTouchEnabled(enabled);
		}
	}

	@Override
	public void setTogglePanelText(boolean isExpanded) {
		if (isExpanded) {
			mExpandCollapseButton.setText(R.string.button_collapse);
			mExpandCollapseButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_down, 0, 0, 0);
		} else {
			mExpandCollapseButton.setText(R.string.button_more_info);
			mExpandCollapseButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_up, 0, 0, 0);
		}
	}

	@Override
	public void showRating(int rating) {
		mRatingStarHolder.setRating(rating);
	}

	@Override
	public void showLocationLocality(String locality) {
		mLocationLocality.setVisibility(View.VISIBLE);
		mLocationLocality.setText(locality);
	}

	@Override
	public void hideLocationLocality() {
		mLocationLocality.setVisibility(View.GONE);
	}

	@Override
	public void showLocationCountryAndContinent(String countryAndContinent) {
		mLocationCountryContinent.setVisibility(View.VISIBLE);
		mLocationCountryContinent.setText(countryAndContinent);
	}

	@Override
	public void hideLocationCountryAndContinent() {
		mLocationCountryContinent.setVisibility(View.GONE);
	}

	@Override
	public void showRatingCount(String ratingCount) {
		mRatingCount.setText(ratingCount);
	}

	@Override
	public void showCommentsCount(String commentsCount) {
		mCommentsCount.setText(commentsCount);
	}

	@Override
	public void showWaitingStatistic(WaitingStatistic waitingStatistic) {
		mWaitingStats.setVisibility(View.VISIBLE);
		mWaitingAverage.setText(waitingStatistic.getAverageTextual());
		StringBuilder waitingStatsText = new StringBuilder();
		waitingStatsText.append(getString(R.string.label_average_colon));
		waitingStatsText.append(waitingStatistic.getAverageTextual());
		waitingStatsText.append(getString(R.string.label_coma_different_times));
		waitingStatsText.append(waitingStatistic.getDifferentTimesCount());
		mWaitingStats.setText(waitingStatsText.toString());
	}

	@Override
	public void hideWaitingStatistic() {
		mWaitingStats.setVisibility(View.GONE);
		mWaitingAverage.setText("-");
	}

	@Override
	public void showDescriptions(Collection<PlaceDescription> descriptions) {
		mDescriptionsHolder.setVisibility(View.VISIBLE);
		mDescriptionsHolderDivider.setVisibility(View.VISIBLE);
		if (mDescriptionsHolder.getChildCount() > 1) {
			mDescriptionsHolder.removeViews(1, mDescriptionsHolder.getChildCount() - 1);
		}
		int counter = 0;
		for (PlaceDescription description : descriptions) {
			counter++;
			DescriptionLayout descriptionLayout = (DescriptionLayout) mLayoutInflater.inflate(R.layout.layout_detail_description, null);
			descriptionLayout.setDescription(description, counter == descriptions.size());
			mDescriptionsHolder.addView(descriptionLayout);
		}
	}

	@Override
	public void hideDescriptionsHolder() {
		mDescriptionsHolder.setVisibility(View.GONE);
		mDescriptionsHolderDivider.setVisibility(View.GONE);
	}

	@Override
	public void showComments(List<Comment> comments) {
		mCommentsHolder.setVisibility(View.VISIBLE);
		mCommentsHolderDivider.setVisibility(View.VISIBLE);
		if (mCommentsHolder.getChildCount() > 1) {
			mCommentsHolder.removeViews(1, mCommentsHolder.getChildCount() - 1);
		}
		int counter = 0;
		for (Comment comment : comments) {
			counter++;
			CommentLayout commentLayout = (CommentLayout) mLayoutInflater.inflate(R.layout.layout_detail_comment, null);
			commentLayout.setComment(comment, counter == comments.size());
			mCommentsHolder.addView(commentLayout);
		}
	}

	@Override
	public void hideCommentsHolder() {
		mCommentsHolder.setVisibility(View.GONE);
		mCommentsHolderDivider.setVisibility(View.GONE);
	}

	@Override
	public boolean isNetworkEnabled() {
		return NetworkUtil.isNetworkEnabled(getContext());
	}

	@Override
	public void notifyInternetConnectionNotAvailable() {
		NotificationManager.showMessage(getContext(), getString(R.string.message_internet_not_available));
	}

	@Override
	public void navigateToStreetView(double latitude, double longitude) {
		Navigator.startStreetViewActivity(getContext(), latitude, longitude);
	}
}
