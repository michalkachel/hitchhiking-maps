package pl.elabo.hitchhikingmaps.ui.adapter;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

public class PlansRecyclerAdapter extends RecyclerView.Adapter<PlansRecyclerAdapter.PlanViewHolder> implements View.OnClickListener {

	private List<Plan> mPlans = new ArrayList<>();
	private OnItemClickListener mOnItemClickListener;

	public PlansRecyclerAdapter() {
	}

	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		this.mOnItemClickListener = onItemClickListener;
	}

	@Override
	public PlanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_plan, parent, false);
		view.setOnClickListener(this);
		return new PlanViewHolder(view);
	}

	@Override
	public void onBindViewHolder(PlanViewHolder holder, int position) {
		Plan plan = mPlans.get(position);
		holder.mPlanName.setText(plan.getName());
		holder.mPlacesCount.setText(String.valueOf(plan.getPlacesCount()));
		holder.mMapOffline.setVisibility(plan.isMapDownloaded() ? View.VISIBLE : View.GONE);

		holder.itemView.setTag(plan);
	}

	@Override
	public int getItemCount() {
		return mPlans.size();
	}

	@Override
	public void onClick(final View v) {
		// Give some time to the ripple to finish the effect
		if (mOnItemClickListener != null) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					mOnItemClickListener.onItemClick(v, ((Plan) v.getTag()));
				}
			}, 200);
		}
	}

	public void setPlans(List<Plan> plans) {
		mPlans.clear();
		mPlans.addAll(plans);
		notifyDataSetChanged();
	}

	public interface OnItemClickListener {
		void onItemClick(View view, Plan plan);
	}

	protected static class PlanViewHolder extends RecyclerView.ViewHolder {
		@Bind(R.id.plan_name)
		public TextView mPlanName;
		@Bind(R.id.plan_places_count)
		public TextView mPlacesCount;
		@Bind(R.id.plan_map_offline)
		public TextView mMapOffline;


		public PlanViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
	}
}
