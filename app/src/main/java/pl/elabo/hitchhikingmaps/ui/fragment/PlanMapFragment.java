package pl.elabo.hitchhikingmaps.ui.fragment;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.hannesdorfmann.fragmentargs.annotation.Arg;

import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.OverlayManager;

import butterknife.Bind;
import butterknife.OnClick;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.mvp.presenter.PlanMapPresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.PlanMapPresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.PlanMapView;
import pl.elabo.hitchhikingmaps.ui.activity.base.BaseActivity;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BaseMapFragment;
import pl.elabo.hitchhikingmaps.ui.service.MapDownloadService;
import pl.elabo.hitchhikingmaps.util.MapUtil;
import pl.elabo.hitchhikingmaps.util.NetworkUtil;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 22.07.2015.
 */
public class PlanMapFragment extends BaseMapFragment<PlanMapPresenter> implements PlanMapView {

	@Arg
	long mPlanId;

	@Bind(R.id.download_map)
	FloatingActionButton mDownloadMapButton;

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_plan_map;
	}

	@Override
	protected PlanMapPresenter getMapPresenter() {
		return new PlanMapPresenterImpl(this, mPlanId);
	}

	@Override
	protected int getMapMinZoomLevel() {
		return AppConstants.Map.PLAN_MIN_ZOOM_LEVEL;
	}

	@Override
	protected void setInitialMapPosition(MapView mapView) {
		//map position will be set while plan places are fetched
	}

	@Override
	public void setTitle(String title) {
		if (getActivity() != null) {
			((BaseActivity) getActivity()).setToolbarTitle(title);
		}
	}

	@Override
	public void zoomToBoundingBox(final BoundingBoxE6 boundingBoxE6) {
		MapUtil.zoomToBoundingBox(mMapView, boundingBoxE6);
	}

	@Override
	public void setDownloadMapButtonVisibility(boolean visible) {
		if (mDownloadMapButton != null) {
			mDownloadMapButton.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}

	@Override
	public void askForPlanDeleting() {
		new AlertDialog.Builder(getActivity())
				.setTitle(getString(R.string.title_delete_plan))
				.setMessage(getString(R.string.question_delete_plan))
				.setPositiveButton(getActivity().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mMapPresenter.onConfirmPlanDeleting();
						dialog.dismiss();
					}
				})
				.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.setCancelable(true)
				.show();
	}

	@Override
	public void notifyPlanDeleted(String planName) {
		NotificationManager.showMessage(getContext(), getString(R.string.message_plan_deleted) + planName);
	}

	@Override
	public void notifyMapStartedToDownload() {
		NotificationManager.showMessage(getContext(), getString(R.string.message_plan_map_started_to_download));
	}

	@Override
	public void notifyMapAlreadyDownloaded() {
		NotificationManager.showMessage(getContext(), getString(R.string.message_plan_map_downloaded));
	}

	@Override
	public void notifyInternetConnectionNotAvailable() {
		NotificationManager.showMessage(getContext(), getString(R.string.message_internet_not_available));
	}

	@Override
	public void showMapWithoutPlanPlaces() {
		mMapView.getController().setZoom(mMapView.getMinZoomLevel());
		final OverlayManager overlayManager = mMapView.getOverlayManager();
		if (overlayManager.size() > mMarkedItemOverlayIndex) {
			overlayManager.remove(mMarkedItemOverlayIndex);
		}
	}

	@Override
	public void downloadMap(long planId) {
		MapDownloadService.startDownloading(getContext(), planId);
	}

	@Override
	public boolean isNetworkEnabled() {
		return NetworkUtil.isNetworkEnabled(getContext());
	}

	@Override
	public void closePlan() {
		getActivity().finish();
	}

	public void onPlaceRemovedFromPlan(long planId, long remotePlaceId) {
		mMapPresenter.onPlaceRemovedFromPlan(planId, remotePlaceId);
	}

	@OnClick(R.id.download_map)
	void onDownloadMap() {
		mMapPresenter.onDownloadMap();
	}

	@OnClick(R.id.delete_plan)
	void onDeletePlan() {
		mMapPresenter.onDeletePlan();
	}
}
