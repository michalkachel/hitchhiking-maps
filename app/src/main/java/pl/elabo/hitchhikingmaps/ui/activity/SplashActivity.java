package pl.elabo.hitchhikingmaps.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.Navigator;
import pl.elabo.hitchhikingmaps.listener.OnFragmentDetachListener;
import pl.elabo.hitchhikingmaps.mvp.presenter.SplashPresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.SplashPresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.SplashView;
import pl.elabo.hitchhikingmaps.ui.activity.base.BaseActivity;
import pl.elabo.hitchhikingmaps.ui.fragment.TutorialFragment;
import pl.elabo.hitchhikingmaps.util.NotificationManager;
import pl.elabo.hitchhikingmaps.util.SharedPreferencesManager;


public class SplashActivity extends BaseActivity implements SplashView, OnFragmentDetachListener {

	@Bind(R.id.progress_bar)
	protected ProgressBar mProgressBar;
	@Bind(R.id.progress_text)
	protected TextView mProgressText;
	@Bind(R.id.tutorial_button_holder)
	protected ViewGroup mTutorialButtonHolder;
	@Bind(R.id.show_map_button)
	protected Button mShowMapButton;

	private SplashPresenter mSplashPresenter;

	@Override
	protected int getLayoutId() {
		return R.layout.activity_splash;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (mSplashPresenter == null) {
			mSplashPresenter = new SplashPresenterImpl(this);
		}

		mSplashPresenter.onCreate();

	}

	@Override
	protected void onResume() {
		super.onResume();
		mSplashPresenter.onResume();
	}

	@Override
	public boolean wasFirstUpdate() {
		return SharedPreferencesManager.wasFirstUpdate(this);
	}

	@Override
	public void navigateToGlobalMap() {
		Navigator.startMapActivity(this);
		finish();
	}

	@Override
	public void setWasFirstUpdate(boolean wasFirstUpdate) {
		SharedPreferencesManager.setWasFirstUpdate(this, wasFirstUpdate);
	}

	@Override
	public void showProgress() {
		mProgressBar.setVisibility(View.VISIBLE);
		mProgressText.setVisibility(View.VISIBLE);
		mTutorialButtonHolder.setVisibility(View.VISIBLE);
	}

	@Override
	public List<InputStream> getJsonInputs() throws IOException {
		List<InputStream> inputStreams = new ArrayList<>();

		inputStreams.add(getAssets().open("af.json"));
		inputStreams.add(getAssets().open("an.json"));
		inputStreams.add(getAssets().open("as.json"));
		inputStreams.add(getAssets().open("eu.json"));
		inputStreams.add(getAssets().open("oc.json"));
		inputStreams.add(getAssets().open("sa.json"));
		inputStreams.add(getAssets().open("na.json"));

		return inputStreams;
	}

	@Override
	public List<String> getContinentsNames() {
		List<String> continentsNames = new ArrayList<>();

		continentsNames.add(getString(R.string.continent_africa));
		continentsNames.add(getString(R.string.continent_antarctica));
		continentsNames.add(getString(R.string.continent_asia));
		continentsNames.add(getString(R.string.continent_europe));
		continentsNames.add(getString(R.string.continent_australia));
		continentsNames.add(getString(R.string.continent_south_america));
		continentsNames.add(getString(R.string.continent_north_america));

		return continentsNames;
	}

	@Override
	public void updateProgress(String continent, int progress, int max) {
		mProgressText.setText(String.format("%s%s %d/%d", getString(R.string.message_unpacking_spots_for), continent, progress, max));
		mProgressBar.setMax(max);
		mProgressBar.setProgress(progress);
	}

	@Override
	public void showTutorial() {
		getSupportFragmentManager().beginTransaction().add(R.id.tutrial_container, new TutorialFragment(), TutorialFragment.FRAGMENT_TAG).addToBackStack(null).commit();
	}

	@Override
	public void showShowMapButton() {
		mShowMapButton.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean isTutorialShowing() {
		return getSupportFragmentManager().findFragmentByTag(TutorialFragment.FRAGMENT_TAG) != null;
	}

	@Override
	protected void onPause() {
		super.onPause();
		mSplashPresenter.onPause();
	}

	@Override
	protected void onDestroy() {
		mSplashPresenter.onDestroy();
		super.onDestroy();
	}

	@Override
	public void showError(Throwable throwable) {
		NotificationManager.showMessage(SplashActivity.this, throwable.getMessage());
	}

	@OnClick(R.id.tutorial_button)
	void onShowTutorial() {
		mSplashPresenter.onShowTutorial();
	}

	@Override
	public void onFragmentDetached() {
		mSplashPresenter.onFragmentDetached();
	}

}
