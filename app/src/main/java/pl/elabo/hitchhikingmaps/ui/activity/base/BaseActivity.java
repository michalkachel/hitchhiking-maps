package pl.elabo.hitchhikingmaps.ui.activity.base;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;

import com.google.android.gms.analytics.GoogleAnalytics;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.Navigator;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 20.07.2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

	protected ActionBarDrawerToggle mDrawerToggle;
	@Nullable
	@Bind(R.id.navigation_view)
	protected NavigationView mNavigationView;
	@Nullable
	@Bind(R.id.toolbar)
	protected Toolbar mToolbar;
	@Nullable
	@Bind(R.id.drawer)
	DrawerLayout mDrawer;

	protected boolean mWasBackPressed = false;

	@LayoutRes
	protected abstract int getLayoutId();

	public void setToolbarTitle(String title) {
		if (mToolbar != null) {
			mToolbar.setTitle(title);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutId());
		handleArguments();
		ButterKnife.bind(this);

		if (mDrawer != null && mToolbar != null && hasDrawerToggle()) {
			mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.drawer_open, R.string.drawer_close);
			mDrawer.setDrawerListener(mDrawerToggle);
		} else {
			if (getSupportActionBar() != null) {
				getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			}
		}

		if (mNavigationView != null) {
			mNavigationView.setNavigationItemSelectedListener(this);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		mWasBackPressed = false;
		if (mNavigationView != null) {
			setNavigationItemSelected();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	protected void handleArguments() {
	}

	protected void setNavigationItemSelected() {
	}

	@Override
	public void onBackPressed() {
		if (mDrawer != null && mDrawer.isDrawerOpen(Gravity.START)) {
			mDrawer.closeDrawers();
		} else if (isRootActivity()) {
			if (mWasBackPressed) {
				super.onBackPressed();
			} else {
				mWasBackPressed = true;
				NotificationManager.showMessage(this, getString(R.string.message_press_again_to_exit));
			}
		} else {
			super.onBackPressed();
		}
	}

	protected boolean isRootActivity() {
		return false;
	}

	protected boolean hasDrawerToggle() {
		return true;
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (mDrawerToggle != null) {
			mDrawerToggle.syncState();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (mDrawerToggle != null) {
			mDrawerToggle.onConfigurationChanged(newConfig);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		} else if (item.getItemId() == android.R.id.home) {
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case R.id.drawer_map:
				Navigator.startMapActivity(this);
				break;
			case R.id.drawer_plans:
				Navigator.startPlansActivity(this);
				break;
			case R.id.drawer_update:
				Navigator.startUpdateActivity(this);
				break;
			case R.id.drawer_about:
				Navigator.startAboutActivity(this);
				break;
			case R.id.drawer_tutorial:
				Navigator.startTutorialActivity(this);
				break;
		}
		if (mDrawer != null) {
			mDrawer.closeDrawers();
		}
		return false;
	}
}
