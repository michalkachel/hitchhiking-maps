package pl.elabo.hitchhikingmaps.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.listener.OnFragmentDetachListener;
import pl.elabo.hitchhikingmaps.ui.activity.TutorialActivity;
import pl.elabo.hitchhikingmaps.ui.adapter.TutorialPagerAdapter;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BaseFragment;

/**
 * Created by ELABO on 13.11.15.
 */
public class TutorialFragment extends BaseFragment {
	public static final String FRAGMENT_TAG = TutorialFragment.class.getName();

	OnFragmentDetachListener mOnFragmentDetachListener;

	@Bind(R.id.tutorial_pager)
	ViewPager mTutorialPager;

	@Bind(R.id.indicator)
	CirclePageIndicator mIndicator;

	@Bind(R.id.close)
	View mClose;

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_tutorial;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnFragmentDetachListener) {
			mOnFragmentDetachListener = (OnFragmentDetachListener) context;
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (getActivity() instanceof TutorialActivity) {
			mClose.setVisibility(View.GONE);
		}

		List<Pair<Integer, Integer>> pagePairs = new ArrayList<>();
		pagePairs.add(new Pair<Integer, Integer>(R.drawable.tuto1, R.string.text_tutorial_1));
		pagePairs.add(new Pair<Integer, Integer>(R.drawable.tuto2, R.string.text_tutorial_2));
		pagePairs.add(new Pair<Integer, Integer>(R.drawable.tuto3, R.string.text_tutorial_3));
		pagePairs.add(new Pair<Integer, Integer>(R.drawable.tuto4, R.string.text_tutorial_4));

		TutorialPagerAdapter tutorialPagerAdapter = new TutorialPagerAdapter((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE), pagePairs);
		mTutorialPager.setAdapter(tutorialPagerAdapter);
		mIndicator.setViewPager(mTutorialPager);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		if (mOnFragmentDetachListener != null) {
			mOnFragmentDetachListener.onFragmentDetached();
		}
	}

	@OnClick(R.id.close)
	void onCloseTutorial() {
		getActivity().onBackPressed();
	}
}
