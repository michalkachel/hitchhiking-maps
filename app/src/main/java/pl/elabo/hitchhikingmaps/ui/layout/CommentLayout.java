package pl.elabo.hitchhikingmaps.ui.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.model.entity.Comment;
import pl.elabo.hitchhikingmaps.model.entity.User;
import pl.elabo.hitchhikingmaps.util.DateUtil;

/**
 * Created by michalkachel on 17.08.2015.
 */
public class CommentLayout extends LinearLayout {

	@Bind(R.id.comment)
	TextView mComment;

	@Bind(R.id.author)
	TextView mAuthor;

	@Bind(R.id.date)
	TextView mDate;

	@Bind(R.id.divider)
	View mDivider;

	public CommentLayout(Context context) {
		super(context);
	}

	public CommentLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CommentLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public CommentLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		ButterKnife.bind(this);
	}

	public void setComment(Comment comment, boolean isLast) {
		mComment.setText(comment.getComment());
		mDate.setText(DateUtil.formatDate(comment.getDatetime()));
		final User user = comment.getUser();
		if (user != null && user.getName() != null) {
			mAuthor.setText(user.getName());
		} else {
			mAuthor.setVisibility(View.GONE);
		}
		if (isLast) {
			mDivider.setVisibility(View.GONE);
		}
	}
}
