package pl.elabo.hitchhikingmaps.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.hannesdorfmann.fragmentargs.annotation.Arg;

import butterknife.Bind;
import butterknife.OnClick;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.listener.OnPlaceRemovedFromPlanListener;
import pl.elabo.hitchhikingmaps.mvp.presenter.PlanPlaceDetailPresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.PlanPlaceDetailPresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.PlanPlaceDetailView;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BasePlaceDetailFragment;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 22.07.2015.
 */
public class PlanPlaceDetailFragment extends BasePlaceDetailFragment<PlanPlaceDetailPresenter> implements PlanPlaceDetailView {
	public static final String FRAGMENT_TAG = PlanPlaceDetailFragment.class.getName();
	@Arg
	long mPlanId;
	@Bind(R.id.remove_from_plan)
	FloatingActionButton mRemoveFromPlanButton;
	private OnPlaceRemovedFromPlanListener mOnPlaceRemovedFromPlanListener;

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_plan_place_detail;
	}

	@Override
	protected PlanPlaceDetailPresenter getPlaceDetailPresenter() {
		return new PlanPlaceDetailPresenterImpl(this, mPlanId);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		mOnPlaceRemovedFromPlanListener = (OnPlaceRemovedFromPlanListener) context;
	}

	@OnClick(R.id.remove_from_plan)
	void onRemoveFromPlan() {
		mPlaceDetailPresenter.onRemoveFromPlan();
	}

	@Override
	public void showPlaceDetailHolder() {
		super.showPlaceDetailHolder();
		mRemoveFromPlanButton.setVisibility(View.GONE);
		mRemoveFromPlanButton.show();
	}

	@Override
	public void hidePlaceDetailHolder() {
		super.hidePlaceDetailHolder();
		mRemoveFromPlanButton.setVisibility(View.GONE);
	}

	@Override
	public void askForPlaceRemoval() {
		new AlertDialog.Builder(getActivity())
				.setTitle(getString(R.string.title_remove_place_from_plan))
				.setMessage(getString(R.string.question_remove_place_from_plan))
				.setPositiveButton(getActivity().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						mPlaceDetailPresenter.onRemoveFromPlanConfirmed();
					}
				})
				.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.setCancelable(true)
				.show();
	}

	@Override
	public void showPlaceRemovalConfirmation() {
		NotificationManager.showMessage(getContext(), getString(R.string.message_place_removed_from_plan));
	}

	@Override
	public void removeRemovedPlaceFromMap(long planId, long remotePlaceId) {
		if (mOnPlaceRemovedFromPlanListener != null) {
			mOnPlaceRemovedFromPlanListener.onPlaceRemovedFromPlan(planId, remotePlaceId);
		}
	}
}
