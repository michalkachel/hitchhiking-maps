package pl.elabo.hitchhikingmaps.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.Bind;
import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.Navigator;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.mvp.presenter.PlansListPresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.PlansListPresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.PlansListView;
import pl.elabo.hitchhikingmaps.ui.adapter.PlansRecyclerAdapter;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BaseFragment;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 12.10.2015.
 */
public class PlansListFragment extends BaseFragment implements PlansListView, PlansRecyclerAdapter.OnItemClickListener {

	@Bind(R.id.recycler_view)
	RecyclerView mRecyclerView;

	@Bind(R.id.empty_plan)
	ViewGroup mEmptyPlan;

	private PlansListPresenter mPlansListPresenter;
	private PlansRecyclerAdapter mPlansRecyclerAdapter;

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_plans;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mPlansListPresenter = new PlansListPresenterImpl(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		mPlansListPresenter.onResume();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mPlansListPresenter.onViewCreated();
	}

	@Override
	public void initList() {
		LinearLayoutManager manager = new LinearLayoutManager(getActivity(), GridLayoutManager.VERTICAL, false);
		mRecyclerView.setLayoutManager(manager);
		mPlansRecyclerAdapter = new PlansRecyclerAdapter();
		mPlansRecyclerAdapter.setOnItemClickListener(this);
		mRecyclerView.setAdapter(mPlansRecyclerAdapter);
	}

	@Override
	public void showPlans(List<Plan> plans) {
		mPlansRecyclerAdapter.setPlans(plans);
	}

	@Override
	public void navigateToPlan(Plan plan) {
		Navigator.startPlanMap(getActivity(), plan.getId());
	}

	@Override
	public void setEmptyPlansVisibility(boolean visible) {
		mEmptyPlan.setVisibility(visible ? View.VISIBLE : View.GONE);
	}

	@Override
	public void showError(Throwable throwable) {
		NotificationManager.showMessage(getActivity(), throwable.getMessage());
	}

	@Override
	public void onItemClick(View view, Plan plan) {
		mPlansListPresenter.onPlanSelected(plan);
	}
}
