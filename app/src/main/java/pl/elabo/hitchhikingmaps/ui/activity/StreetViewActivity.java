package pl.elabo.hitchhikingmaps.ui.activity;

import android.os.Bundle;
import android.os.Handler;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.ui.activity.base.BaseActivity;
import pl.elabo.hitchhikingmaps.util.NotificationManager;

/**
 * Created by michalkachel on 12.10.2015.
 */
public class StreetViewActivity extends BaseActivity implements OnStreetViewPanoramaReadyCallback {

	public static final int PANORAMA_CHECK_DELAY = 5000;
	public static final int MAX_RADIUS_FROM_LATLNG_IN_METERS = 100;

	private StreetViewPanoramaFragment mStreetViewPanoramaFragment;
	private StreetViewPanorama mStreetViewPanorama;

	private double mLatitude;
	private double mLongitude;

	private Handler mHandler = new Handler();
	private Runnable mCheckRunnable = new Runnable() {
		@Override
		public void run() {
			if (mStreetViewPanorama == null || mStreetViewPanorama.getLocation() == null) {
				NotificationManager.showMessage(StreetViewActivity.this, getString(R.string.message_street_view_failed));
				finish();
			}
		}
	};

	@Override
	protected int getLayoutId() {
		return R.layout.activity_street_view;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}

		mStreetViewPanoramaFragment = (StreetViewPanoramaFragment) getFragmentManager().findFragmentById(R.id.streetviewpanorama);
		mStreetViewPanoramaFragment.getStreetViewPanoramaAsync(this);

	}

	@Override
	protected void handleArguments() {
		super.handleArguments();
		mLatitude = getIntent().getExtras().getDouble(AppConstants.Extras.LATITUDE);
		mLongitude = getIntent().getExtras().getDouble(AppConstants.Extras.LONGITUDE);
	}

	@Override
	public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
		mStreetViewPanorama = streetViewPanorama;
		mStreetViewPanorama.setPosition(new LatLng(mLatitude, mLongitude), MAX_RADIUS_FROM_LATLNG_IN_METERS);

		mHandler.postDelayed(mCheckRunnable, PANORAMA_CHECK_DELAY);
	}

	@Override
	protected void onStop() {
		super.onStop();
		mHandler.removeCallbacks(mCheckRunnable);
	}
}
