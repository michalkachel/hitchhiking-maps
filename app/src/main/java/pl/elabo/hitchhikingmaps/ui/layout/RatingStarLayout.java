package pl.elabo.hitchhikingmaps.ui.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.elabo.hitchhikingmaps.R;

/**
 * Created by michalkachel on 17.08.2015.
 */
public class RatingStarLayout extends LinearLayout {

	@Bind({R.id.star_1, R.id.star_2, R.id.star_3, R.id.star_4, R.id.star_5})
	List<ImageView> mStarImageViews;

	public RatingStarLayout(Context context) {
		super(context);
	}

	public RatingStarLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RatingStarLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public RatingStarLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		ButterKnife.bind(this);
	}

	public void setRating(int rating) {

		for (int i = 0; i < mStarImageViews.size(); i++) {
			if (i < rating) {
				mStarImageViews.get(i).setImageResource(R.drawable.star);
			} else {
				mStarImageViews.get(i).setImageResource(R.drawable.star_outline);
			}
		}

	}
}
