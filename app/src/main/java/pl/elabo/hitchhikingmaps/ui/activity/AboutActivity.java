package pl.elabo.hitchhikingmaps.ui.activity;

import android.os.Bundle;

import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.ui.activity.base.BaseActivity;

/**
 * Created by ELABO on 12.11.15.
 */
public class AboutActivity extends BaseActivity {

	@Override
	protected int getLayoutId() {
		return R.layout.activity_about;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			getSupportActionBar().setTitle(R.string.title_about);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}
	}

	@Override
	protected boolean hasDrawerToggle() {
		return false;
	}

	@Override
	protected void setNavigationItemSelected() {
		mNavigationView.setCheckedItem(R.id.drawer_about);
	}
}
