package pl.elabo.hitchhikingmaps.ui.fragment;

import android.os.Handler;

import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;

import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.mvp.presenter.GlobalMapPresenter;
import pl.elabo.hitchhikingmaps.mvp.presenter.GlobalMapPresenterImpl;
import pl.elabo.hitchhikingmaps.mvp.view.GlobalMapView;
import pl.elabo.hitchhikingmaps.ui.fragment.base.BaseMapFragment;

/**
 * Created by michalkachel on 22.07.2015.
 */
public class GlobalMapFragment extends BaseMapFragment<GlobalMapPresenter> implements GlobalMapView {

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_map;
	}

	@Override
	public void initializeMyLocation() {
		super.initializeMyLocation();
		mLocationOverlay.enableMyLocation();
		mLocationOverlay.enableFollowLocation();
	}

	@Override
	public void initializeMapChangeListener() {
		mMapView.setMapListener(new MapListener() {
			@Override
			public boolean onScroll(ScrollEvent event) {
				mMapPresenter.onMapChanged(mMapView.getBoundingBox());
				return false;
			}

			@Override
			public boolean onZoom(ZoomEvent event) {
				mMapPresenter.onMapChanged(mMapView.getBoundingBox());
				return false;
			}
		});
	}

	@Override
	public void invokeDelayedOnMapChanged(long delayInMillis) {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (mMapView != null && mMapPresenter != null) {
					mMapPresenter.onRefreshMapNow(mMapView.getBoundingBox());
				}
			}
		}, delayInMillis);
	}

	@Override
	protected GlobalMapPresenter getMapPresenter() {
		return new GlobalMapPresenterImpl(this);
	}
}
