package pl.elabo.hitchhikingmaps.ui.activity;

import android.os.Bundle;
import android.view.View;

import pl.elabo.hitchhikingmaps.R;
import pl.elabo.hitchhikingmaps.application.AppConstants;
import pl.elabo.hitchhikingmaps.listener.OnPlaceRemovedFromPlanListener;
import pl.elabo.hitchhikingmaps.ui.fragment.PlanMapFragment;
import pl.elabo.hitchhikingmaps.ui.fragment.PlanMapFragmentBuilder;
import pl.elabo.hitchhikingmaps.ui.fragment.PlanPlaceDetailFragment;
import pl.elabo.hitchhikingmaps.ui.fragment.PlanPlaceDetailFragmentBuilder;

/**
 * Created by michalkachel on 22.07.2015.
 */
public class PlanMapActivity extends MainActivity implements OnPlaceRemovedFromPlanListener {

	private long mPlanId;

	@Override
	protected boolean isRootActivity() {
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (mToolbar != null) {
			mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
			mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					finish();
				}
			});
		}
	}

	@Override
	protected void handleArguments() {
		super.handleArguments();
		mPlanId = getIntent().getExtras().getLong(AppConstants.Extras.PLAN_ID);
	}

	@Override
	protected void setNavigationItemSelected() {
		mNavigationView.setCheckedItem(R.id.drawer_plans);
	}

	@Override
	public void initializeMap() {
		mMapFragment = PlanMapFragmentBuilder.newPlanMapFragment(mPlanId);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.map_container, mMapFragment, PlanMapFragment.FRAGMENT_TAG)
				.commit();
	}

	@Override
	public void initializePlaceDetail() {
		mPlaceDetailFragment = PlanPlaceDetailFragmentBuilder.newPlanPlaceDetailFragment(mPlanId);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.place_detail_container, mPlaceDetailFragment, PlanPlaceDetailFragment.FRAGMENT_TAG)
				.commit();
	}

	@Override
	public void onPlaceRemovedFromPlan(long planId, long remotePlaceId) {
		((PlanMapFragment) mMapFragment).onPlaceRemovedFromPlan(planId, remotePlaceId);
	}

	@Override
	protected boolean hasDrawerToggle() {
		return false;
	}
}
