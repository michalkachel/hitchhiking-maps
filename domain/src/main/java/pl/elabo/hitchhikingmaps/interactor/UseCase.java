package pl.elabo.hitchhikingmaps.interactor;

/**
 * Created by michalkachel on 10.07.2015.
 */
public interface UseCase {

	void onError(Throwable throwable);
}
