package pl.elabo.hitchhikingmaps.interactor.places;

import pl.elabo.hitchhikingmaps.interactor.UseCase;
import pl.elabo.hitchhikingmaps.model.entity.Continent;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface UpdateAllPlacesUseCase extends UseCase {

	void execute();

	void interrupt();

	void onSuccess();

	void downloadAndUpdatePlaces(Continent continent);
}
