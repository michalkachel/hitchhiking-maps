package pl.elabo.hitchhikingmaps.interactor.places;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;

import java.util.List;

import pl.elabo.hitchhikingmaps.callback.places.GetPlacesFromBoundingBoxCallback;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import timber.log.Timber;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class GetPLacesFromBoundingBoxUseCaseImpl implements GetPlacesFromBoundingBoxUseCase {
	private GetPlacesFromBoundingBoxCallback mCallback;

	public GetPLacesFromBoundingBoxUseCaseImpl(GetPlacesFromBoundingBoxCallback callback) {
		mCallback = callback;
	}

	public void execute(int aLatNorth, int aLatSouth, int aLonWest, int aLonEast) {
		final double latNorth = (double) aLatNorth / 1000000;
		final double latSouth = (double) aLatSouth / 1000000;
		final double lonWest = (double) aLonWest / 1000000;
		final double lonEast = (double) aLonEast / 1000000;

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					long millis = System.currentTimeMillis();

					From select = new Select().from(Place.class)
							.where("latitude < ?", latNorth)
							.and("latitude > ?", latSouth)
							.and("longitude < ?", lonWest)
							.and("longitude > ?", lonEast);

					List<Place> places = select.execute();
					Timber.d("Places size: " + places.size() + ", time " + (System.currentTimeMillis() - millis));
					onSuccess(places);
				} catch (Exception e) {
					e.printStackTrace();
					Timber.e(e.getMessage());
					onError(e);
				}
			}
		}).start();
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(List<Place> places) {
		if (mCallback != null) {
			mCallback.onPlacesFromBoundingBoxReceived(places);
		}
	}
}
