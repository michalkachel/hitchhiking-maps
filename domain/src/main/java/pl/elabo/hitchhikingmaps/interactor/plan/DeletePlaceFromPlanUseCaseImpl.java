package pl.elabo.hitchhikingmaps.interactor.plan;

import com.activeandroid.query.Delete;

import pl.elabo.hitchhikingmaps.callback.plan.DeletePlaceFromPlanCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.model.entity.PlanPlace;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class DeletePlaceFromPlanUseCaseImpl implements DeletePlaceFromPlanUseCase {
	private DeletePlaceFromPlanCallback mCallback;

	public DeletePlaceFromPlanUseCaseImpl(DeletePlaceFromPlanCallback callback) {
		mCallback = callback;
	}

	public void execute(final long planId, final long placeRemoteId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					new Delete().from(PlanPlace.class).where("plan_id = ?", planId).and("place_remote_id = ?", placeRemoteId).execute();

					Plan plan = Plan.load(Plan.class, planId);
					plan.loadPlaces();
					onSuccess(plan, placeRemoteId);

				} catch (Exception e) {
					e.printStackTrace();
					onError(e);
				}
			}
		}).start();
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(Plan plan, long placeRemoteId) {
		if (mCallback != null) {
			mCallback.onPlaceDeletedFromPlan(plan, placeRemoteId);
		}
	}
}
