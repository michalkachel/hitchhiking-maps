package pl.elabo.hitchhikingmaps.interactor.plan;

import pl.elabo.hitchhikingmaps.interactor.UseCase;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface CreateNewPlanUseCase extends UseCase {
	void onSuccess(Plan plan);
}
