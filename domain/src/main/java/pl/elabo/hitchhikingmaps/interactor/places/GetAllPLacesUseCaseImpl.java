package pl.elabo.hitchhikingmaps.interactor.places;

import com.activeandroid.query.Select;

import java.util.List;

import pl.elabo.hitchhikingmaps.callback.places.GetAllPlacesCallback;
import pl.elabo.hitchhikingmaps.model.entity.Place;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class GetAllPLacesUseCaseImpl implements GetAllPlacesUseCase {
	private GetAllPlacesCallback mCallback;

	public GetAllPLacesUseCaseImpl(GetAllPlacesCallback callback) {
		mCallback = callback;
	}

	public void execute() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					List<Place> allPlaces = new Select().from(Place.class).limit(1000).execute();
					onSuccess(allPlaces);
				} catch (Exception e) {
					onError(e);
				}
			}
		}).start();
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(List<Place> places) {
		if (mCallback != null) {
			mCallback.onAllPlacesReceived(places);
		}
	}
}
