package pl.elabo.hitchhikingmaps.interactor.places;

import com.activeandroid.ActiveAndroid;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import pl.elabo.hitchhikingmaps.callback.places.InitialInsertAllPlacesCallback;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import pl.elabo.hitchhikingmaps.model.rest.RestClient;
import timber.log.Timber;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class InitialInsertAllPlacesUseCaseImpl implements InitialInsertAllPlacesUseCase {
	private static InitialInsertAllPlacesUseCase sInitialInsertAllPlacesUseCase;

	private InitialInsertAllPlacesCallback mCallback;

	private List<String> mJsonStrings;

	public static InitialInsertAllPlacesUseCase getInstance() {
		if (sInitialInsertAllPlacesUseCase == null) {
			sInitialInsertAllPlacesUseCase = new InitialInsertAllPlacesUseCaseImpl();
		}
		return sInitialInsertAllPlacesUseCase;
	}

	private InitialInsertAllPlacesUseCaseImpl() {
	}

	public void execute() {
		if (mJsonStrings == null) {
			onError(new Throwable("Call setJsonStrings(List<String> jsonStrings) before executing use case"));
		} else {
			Runnable r = new Runnable() {
				@Override
				public void run() {
					ActiveAndroid.beginTransaction();
					try {
						int index = 0;
						for (final String jsonString : mJsonStrings) {
							Place[] placesArray = RestClient.getInstance().getGson().fromJson(jsonString, Place[].class);
							List<Place> places = Arrays.asList(placesArray);
							int progress = 0;
							int max = places.size();
							for (Place place : places) {
								place.save();
								mCallback.onProgressChanged(index, ++progress, max);
							}
							index++;
							Timber.d("Places inserted: " + places.size());
						}
						ActiveAndroid.setTransactionSuccessful();
						Timber.d("Transaction successful");
						onSuccess();
					} catch (Exception e) {
						e.printStackTrace();
						onError(e);
					} finally {
						ActiveAndroid.endTransaction();
					}
				}

			};

			new Thread(r).start();
		}
	}

	@Override
	public void onSuccess() {
		if (mCallback != null) {
			mCallback.onAllPlacesInserted();
		}
	}

	@Override
	public void removeCallbackReference() {
		mCallback = null;
	}

	@Override
	public void setCallbackReference(InitialInsertAllPlacesCallback callback) {
		mCallback = callback;
	}

	@Override
	public void setJsonStrings(List<String> jsonStrings) {
		mJsonStrings = jsonStrings;
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}
}
