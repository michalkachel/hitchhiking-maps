package pl.elabo.hitchhikingmaps.interactor.places;

import java.util.List;

import pl.elabo.hitchhikingmaps.interactor.UseCase;
import pl.elabo.hitchhikingmaps.model.entity.Place;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface GetAllPlacesUseCase extends UseCase {
	void onSuccess(List<Place> places);
}
