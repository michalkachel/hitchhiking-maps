package pl.elabo.hitchhikingmaps.interactor.plan;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import pl.elabo.hitchhikingmaps.callback.plan.GetPlanCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class GetPlanUseCaseImpl implements GetPlanUseCase {
	private GetPlanCallback mCallback;
	private long mPlanId;

	public GetPlanUseCaseImpl(GetPlanCallback callback, long planId) {
		mCallback = callback;
		mPlanId = planId;
	}

	public void execute() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Plan plan = new Select().from(Plan.class).where(Table.DEFAULT_ID_NAME + "= ?", mPlanId).executeSingle();
					onSuccess(plan);
				} catch (Exception e) {
					e.printStackTrace();
					onError(e);
				}
			}
		}).start();
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(Plan plan) {
		if (mCallback != null) {
			mCallback.onPlanReceived(plan);
		}
	}
}
