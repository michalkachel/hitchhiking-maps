package pl.elabo.hitchhikingmaps.interactor.plan;

import pl.elabo.hitchhikingmaps.callback.plan.DeletePlanCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class DeletePlanUseCaseImpl implements DeletePlanUseCase {
	private DeletePlanCallback mCallback;

	public DeletePlanUseCaseImpl(DeletePlanCallback callback) {
		mCallback = callback;
	}

	public void execute(Plan plan) {
		try {
			plan.deleteWithPlaces();
			onSuccess(plan.getName());
		} catch (Exception e) {
			e.printStackTrace();
			onError(e);
		}
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(String planName) {
		if (mCallback != null) {
			mCallback.onPlanDeleted(planName);
		}
	}
}
