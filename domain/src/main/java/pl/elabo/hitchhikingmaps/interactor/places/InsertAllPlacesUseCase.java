package pl.elabo.hitchhikingmaps.interactor.places;

import pl.elabo.hitchhikingmaps.interactor.UseCase;
import pl.elabo.hitchhikingmaps.model.entity.Continent;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface InsertAllPlacesUseCase extends UseCase {
	void onSuccess();

	void downloadAndInsertPlaces(Continent continent);
}
