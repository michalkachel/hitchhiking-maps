package pl.elabo.hitchhikingmaps.interactor.plan;

import java.util.List;

import pl.elabo.hitchhikingmaps.interactor.UseCase;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface GetAllPlansUseCase extends UseCase {
	void onSuccess(List<Plan> plans);
}
