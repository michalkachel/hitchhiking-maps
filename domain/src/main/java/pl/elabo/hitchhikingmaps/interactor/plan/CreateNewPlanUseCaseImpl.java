package pl.elabo.hitchhikingmaps.interactor.plan;

import pl.elabo.hitchhikingmaps.callback.plan.CreateNewPlanCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class CreateNewPlanUseCaseImpl implements CreateNewPlanUseCase {
	private CreateNewPlanCallback mCallback;

	public CreateNewPlanUseCaseImpl(CreateNewPlanCallback callback) {
		mCallback = callback;
	}

	public void execute(String planName) {
		try {
			Plan plan = new Plan(planName);
			plan.save();
			onSuccess(plan);
		} catch (Exception e) {
			onError(e);
		}
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(Plan plan) {
		if (mCallback != null) {
			mCallback.onPlanCreated(plan);
		}
	}
}
