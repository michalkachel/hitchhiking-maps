package pl.elabo.hitchhikingmaps.interactor.plan;

import com.activeandroid.query.Select;

import pl.elabo.hitchhikingmaps.callback.plan.AddPlaceToPlanCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;
import pl.elabo.hitchhikingmaps.model.entity.PlanPlace;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class AddPlaceToPlanUseCaseImpl implements AddPlaceToPlanUseCase {
	private AddPlaceToPlanCallback mCallback;

	public AddPlaceToPlanUseCaseImpl(AddPlaceToPlanCallback callback) {
		mCallback = callback;
	}

	public void execute(final long planId, final long placeRemoteId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					boolean alreadyInPlan;
					if (new Select().from(PlanPlace.class)
							.where("plan_id = ?", planId)
							.and("place_remote_id = ?", placeRemoteId)
							.exists()) {
						alreadyInPlan = true;
					} else {
						new PlanPlace(placeRemoteId, planId).save();
						alreadyInPlan = false;
					}
					Plan plan = Plan.load(Plan.class, planId);
					onSuccess(plan, alreadyInPlan);
				} catch (Exception e) {
					e.printStackTrace();
					onError(e);
				}
			}
		}).start();
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(Plan plan, boolean alreadyInPlan) {
		if (mCallback != null) {
			mCallback.onPlaceAddedToPlan(plan, alreadyInPlan);
		}
	}
}
