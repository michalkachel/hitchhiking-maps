package pl.elabo.hitchhikingmaps.interactor.places;

import java.util.List;

import pl.elabo.hitchhikingmaps.callback.places.InitialInsertAllPlacesCallback;
import pl.elabo.hitchhikingmaps.interactor.UseCase;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface InitialInsertAllPlacesUseCase extends UseCase {

	void execute();

	void onSuccess();

	void removeCallbackReference();

	void setCallbackReference(InitialInsertAllPlacesCallback callback);

	void setJsonStrings(List<String> jsonStrings);

}
