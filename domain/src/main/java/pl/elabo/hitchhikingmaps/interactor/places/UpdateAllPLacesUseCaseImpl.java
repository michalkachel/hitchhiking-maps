package pl.elabo.hitchhikingmaps.interactor.places;

import com.activeandroid.ActiveAndroid;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import pl.elabo.hitchhikingmaps.callback.places.UpdateAllPlacesCallback;
import pl.elabo.hitchhikingmaps.model.entity.Continent;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import pl.elabo.hitchhikingmaps.model.rest.RestClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class UpdateAllPlacesUseCaseImpl implements UpdateAllPlacesUseCase {
	private UpdateAllPlacesCallback mCallback;

	private int mContinentsSize;
	private AtomicBoolean mInterrupted = new AtomicBoolean(false);
	private AtomicBoolean mWasAlreadyFinished = new AtomicBoolean(false);
	private AtomicInteger mDownloadedCounter = new AtomicInteger(0);
	private AtomicInteger mAddedPlacesCounter = new AtomicInteger(0);

	public UpdateAllPlacesUseCaseImpl(UpdateAllPlacesCallback callback) {
		mCallback = callback;
	}

	@Override
	public void execute() {
		RestClient.getInstance().getContinentService().getContinents(new Callback<Map<String, Continent>>() {
			@Override
			public void success(Map<String, Continent> stringContinentMap, Response response) {
				if (mInterrupted.get()) {
					onInterrupted();
				} else {
					mContinentsSize = stringContinentMap.size();
					for (Map.Entry<String, Continent> entry : stringContinentMap.entrySet()) {
						downloadAndUpdatePlaces(entry.getValue());
					}
				}
			}

			@Override
			public void failure(RetrofitError error) {
				onError(error);
			}
		});
	}

	public void downloadAndUpdatePlaces(final Continent continent) {
		RestClient.getInstance().getPlaceService().getPlacesByContinent(continent.getCode(), new Callback<List<Place>>() {
			@Override
			public void success(final List<Place> places, Response response) {
				Runnable r = new Runnable() {
					@Override
					public void run() {
						Timber.d("start " + places.size());
						ActiveAndroid.beginTransaction();
						try {
							for (Place place : places) {
								if (mInterrupted.get()) {
									onInterrupted();
									return;
								}
								if (!Place.doesPlaceExist(place.getRemoteId())) {
									Timber.d("remote_id " + place.getRemoteId() + " haven't exist");
									place.save();
									mAddedPlacesCounter.incrementAndGet();
								}
							}
							ActiveAndroid.setTransactionSuccessful();
						} catch (Exception e) {
							mDownloadedCounter.incrementAndGet();
							Timber.e(mDownloadedCounter.get() + " exception " + places.size());
							onError(e);
						} finally {
							ActiveAndroid.endTransaction();
							Timber.d("End Trasnsaction " + places.size());
						}
						mDownloadedCounter.incrementAndGet();
						Timber.d(mDownloadedCounter.get() + " stop " + places.size());
						onSuccess();

					}
				};

				new Thread(r).start();
			}

			@Override
			public void failure(RetrofitError error) {
				onError(error);
			}
		});
	}

	@Override
	public void interrupt() {
		mInterrupted.set(true);
	}

	private void onInterrupted() {
		onFinish(true);
	}

	@Override
	public void onSuccess() {
		if (mDownloadedCounter.get() < mContinentsSize) {
			return;
		}
		onFinish(false);
	}

	private void onFinish(boolean wasInterrupted) {
		if (mCallback != null && !mWasAlreadyFinished.get()) {
			mWasAlreadyFinished.set(true);
			mCallback.onPlacesUpdated(mAddedPlacesCounter.get(), wasInterrupted);
		}
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}
}
