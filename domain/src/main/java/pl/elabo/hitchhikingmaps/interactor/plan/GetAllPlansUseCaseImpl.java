package pl.elabo.hitchhikingmaps.interactor.plan;

import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

import pl.elabo.hitchhikingmaps.callback.plan.GetAllPlansCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class GetAllPlansUseCaseImpl implements GetAllPlansUseCase {
	private GetAllPlansCallback mCallback;

	public GetAllPlansUseCaseImpl(GetAllPlansCallback callback) {
		mCallback = callback;
	}

	public void execute() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					List<Plan> allPlans = new Select().from(Plan.class).orderBy(Table.DEFAULT_ID_NAME + " DESC").execute();
					onSuccess(allPlans);
				} catch (Exception e) {
					e.printStackTrace();
					onError(e);
				}
			}
		}).start();
	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(List<Plan> plans) {
		if (mCallback != null) {
			mCallback.onAllPlansReceived(plans);
		}
	}
}
