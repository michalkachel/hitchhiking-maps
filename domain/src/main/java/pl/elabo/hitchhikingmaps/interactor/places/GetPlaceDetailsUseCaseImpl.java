package pl.elabo.hitchhikingmaps.interactor.places;

import com.activeandroid.query.Select;

import pl.elabo.hitchhikingmaps.callback.places.GetPlaceDetailsCallback;
import pl.elabo.hitchhikingmaps.model.entity.Place;
import pl.elabo.hitchhikingmaps.model.rest.RestClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

/**
 * Created by michalkachel on 11.07.2015.
 */
public class GetPlaceDetailsUseCaseImpl implements GetPlaceDetailsUseCase {
	private GetPlaceDetailsCallback mCallback;

	public GetPlaceDetailsUseCaseImpl(GetPlaceDetailsCallback callback) {
		mCallback = callback;
	}

	public void execute(long placeRemoteId) {
		findPlaceInApi(placeRemoteId);
	}

	@Override
	public void findPlaceInApi(final long remoteId) {
		RestClient.getInstance().getPlaceService().getPlace(remoteId, new Callback<Place>() {
			@Override
			public void success(Place place, Response response) {
				onSuccess(place);
			}

			@Override
			public void failure(RetrofitError error) {
				Timber.e("findPlaceInApiError: " + remoteId, error);
				findPlaceInDatabase(remoteId);
			}
		});
	}

	@Override
	public void findPlaceInDatabase(final long remoteId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Place place = new Select().from(Place.class).where("remote_id = ?", remoteId).executeSingle();
					if (place != null) {
						onSuccess(place);
					} else {
						throw new NullPointerException("place with id " + remoteId + " is not in database");
					}
				} catch (Exception e) {
					onError(e);
				}
			}
		}).start();

	}

	@Override
	public void onError(Throwable throwable) {
		if (mCallback != null) {
			mCallback.onError(throwable);
		}
	}

	@Override
	public void onSuccess(Place place) {
		if (mCallback != null) {
			mCallback.onPlaceDetailsReceived(place);
		}
	}
}
