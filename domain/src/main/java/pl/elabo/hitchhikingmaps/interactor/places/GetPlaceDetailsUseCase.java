package pl.elabo.hitchhikingmaps.interactor.places;

import pl.elabo.hitchhikingmaps.interactor.UseCase;
import pl.elabo.hitchhikingmaps.model.entity.Place;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface GetPlaceDetailsUseCase extends UseCase {
	void onSuccess(Place place);

	void findPlaceInApi(long remoteId);

	void findPlaceInDatabase(long remoteId);
}
