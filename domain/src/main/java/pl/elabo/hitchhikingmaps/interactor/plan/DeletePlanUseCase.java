package pl.elabo.hitchhikingmaps.interactor.plan;

import pl.elabo.hitchhikingmaps.interactor.UseCase;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface DeletePlanUseCase extends UseCase {
	void onSuccess(String planName);
}
