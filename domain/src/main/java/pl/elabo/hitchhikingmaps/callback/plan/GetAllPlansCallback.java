package pl.elabo.hitchhikingmaps.callback.plan;

import java.util.List;

import pl.elabo.hitchhikingmaps.callback.UseCaseCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface GetAllPlansCallback extends UseCaseCallback {
	void onAllPlansReceived(List<Plan> plans);
}
