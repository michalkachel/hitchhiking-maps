package pl.elabo.hitchhikingmaps.callback.plan;

import pl.elabo.hitchhikingmaps.callback.UseCaseCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface DeletePlaceFromPlanCallback extends UseCaseCallback {
	void onPlaceDeletedFromPlan(Plan plan, long placeRemoteId);
}
