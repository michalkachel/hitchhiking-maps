package pl.elabo.hitchhikingmaps.callback;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface UseCaseCallback {
	void onError(Throwable throwable);
}
