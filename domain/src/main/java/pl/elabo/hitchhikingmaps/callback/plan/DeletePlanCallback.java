package pl.elabo.hitchhikingmaps.callback.plan;

import pl.elabo.hitchhikingmaps.callback.UseCaseCallback;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface DeletePlanCallback extends UseCaseCallback {
	void onPlanDeleted(String planName);
}
