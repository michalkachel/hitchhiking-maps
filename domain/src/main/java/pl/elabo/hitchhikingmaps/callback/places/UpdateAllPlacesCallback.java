package pl.elabo.hitchhikingmaps.callback.places;

import pl.elabo.hitchhikingmaps.callback.UseCaseCallback;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface UpdateAllPlacesCallback extends UseCaseCallback {
	void onPlacesUpdated(int addedPlacesCounter, boolean wasInterrupted);
}
