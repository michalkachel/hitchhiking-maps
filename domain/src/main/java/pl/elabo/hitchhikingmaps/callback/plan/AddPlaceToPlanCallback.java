package pl.elabo.hitchhikingmaps.callback.plan;

import pl.elabo.hitchhikingmaps.callback.UseCaseCallback;
import pl.elabo.hitchhikingmaps.model.entity.Plan;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface AddPlaceToPlanCallback extends UseCaseCallback {
	void onPlaceAddedToPlan(Plan plan, boolean alreadyInPlan);
}
