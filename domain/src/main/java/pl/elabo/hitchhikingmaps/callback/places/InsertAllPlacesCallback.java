package pl.elabo.hitchhikingmaps.callback.places;

import pl.elabo.hitchhikingmaps.callback.UseCaseCallback;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface InsertAllPlacesCallback extends UseCaseCallback {
	void onAllPlacesInserted();
}
