package pl.elabo.hitchhikingmaps.callback.places;

import pl.elabo.hitchhikingmaps.callback.UseCaseCallback;
import pl.elabo.hitchhikingmaps.model.entity.Place;

/**
 * Created by michalkachel on 11.07.2015.
 */
public interface GetPlaceDetailsCallback extends UseCaseCallback {
	void onPlaceDetailsReceived(Place place);
}
