package pl.elabo.hitchhikingmaps.model.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by michalkachel on 07.06.2015.
 */
@Table(name = "Places")
public class Place extends Model {

	@Expose
	@SerializedName("id")
	@Column(name = "remote_id", notNull = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
	private long mRemoteId;

	@Expose
	@SerializedName("lat")
	@Column(name = "latitude", notNull = true)
	private double mLatitude;

	@Expose
	@SerializedName("lon")
	@Column(name = "longitude", notNull = true)
	private double mLongitude;

	@Expose
	@SerializedName("rating")
	@Column(name = "rating")
	private int mRating;

	@Column(name = "downloaded")
	private boolean mMapDownloaded;

	@Expose
	@SerializedName("elevation")
	private int mElevation;

	@Expose
	@SerializedName("location")
	private PlaceLocationDescription mPlaceLocationDescription;

	@Expose
	@SerializedName("link")
	private String mWwwLink;

	@Expose
	@SerializedName("datetime")
	private Date mDateTime;

	@Expose
	@SerializedName("rating_stats")
	private RatingStatistic mRatingStatistic;

	@Expose
	@SerializedName("waiting_stats")
	private WaitingStatistic mWaitingStatistic;

	@Expose
	@SerializedName("description")
	private Map<String, PlaceDescription> mDescriptionsMap;

	@Expose
	@SerializedName("comments")
	private List<Comment> mComments;

	@Expose
	@SerializedName("comments_count")
	private int mCommentsCount;

	public Place() {
		super();
	}

	public static Place loadByRemoteId(long remoteId) {
		return new Select().from(Place.class).where("remote_id = ?", remoteId).executeSingle();
	}

	public static boolean doesPlaceExist(long remoteId) {
		return loadByRemoteId(remoteId) != null;
	}

	public int getRating() {
		return mRating;
	}

	public long getRemoteId() {
		return mRemoteId;
	}

	public double getLatitude() {
		return mLatitude;
	}

	public double getLongitude() {
		return mLongitude;
	}

	public PlaceLocationDescription getPlaceLocationDescription() {
		return mPlaceLocationDescription;
	}

	public RatingStatistic getRatingStatistic() {
		return mRatingStatistic;
	}

	public int getCommentsCount() {
		return mCommentsCount;
	}

	public WaitingStatistic getWaitingStatistic() {
		return mWaitingStatistic;
	}

	public Map<String, PlaceDescription> getDescriptionsMap() {
		return mDescriptionsMap;
	}

	public List<Comment> getComments() {
		return mComments;
	}

	public boolean isMapDownloaded() {
		return mMapDownloaded;
	}

	public void setMapDownloaded(boolean mapDownloaded) {
		mMapDownloaded = mapDownloaded;
	}

	@Override
	public String toString() {
		return "Place{" +
				"mRemoteId=" + mRemoteId +
				", mLatitude=" + mLatitude +
				", mLongitude=" + mLongitude +
				", mElevation=" + mElevation +
				", mPlaceLocationDescription=" + mPlaceLocationDescription +
				", mWwwLink='" + mWwwLink + '\'' +
				", mDateTime='" + mDateTime + '\'' +
				", mRating=" + mRating +
				", mRatingStatistic=" + mRatingStatistic +
				", mWaitingStatistic=" + mWaitingStatistic +
				", mDescriptionsMap=" + mDescriptionsMap +
				", mComments=" + mComments +
				", mCommentsCount=" + mCommentsCount +
				'}';
	}
}
