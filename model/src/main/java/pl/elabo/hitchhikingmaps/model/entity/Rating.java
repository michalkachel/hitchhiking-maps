package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 07.06.2015.
 */
public class Rating {

	@Expose
	@SerializedName("rating")
	private int mRating;
	@Expose
	@SerializedName("rating_count")
	private int mRatingCount;

	public Rating() {
	}

	@Override
	public String toString() {
		return "Rating{" +
				"mRating=" + mRating +
				", mRatingCount=" + mRatingCount +
				'}';
	}
}
