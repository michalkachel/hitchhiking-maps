package pl.elabo.hitchhikingmaps.model.rest.service;

import java.util.Map;

import pl.elabo.hitchhikingmaps.model.entity.Continent;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by michalkachel on 16.06.2015.
 */
public interface ContinentService {

	@GET("/?continents")
	void getContinents(Callback<Map<String, Continent>> callback);

}
