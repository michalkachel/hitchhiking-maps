package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 11.06.2015.
 */
public class ApiErrorModel {

	@Expose
	@SerializedName("error")
	private boolean mError;
	@Expose
	@SerializedName("error_description")
	private boolean mErrorDescription;

	public ApiErrorModel() {
	}
}
