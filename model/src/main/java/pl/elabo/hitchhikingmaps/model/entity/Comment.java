package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by michalkachel on 07.06.2015.
 */

public class Comment {

	@Expose
	@SerializedName("id")
	private long mRemoteId;
	@Expose
	@SerializedName("comment")
	private String mComment;
	@Expose
	@SerializedName("datetime")
	private Date mDatetime;
	@Expose
	@SerializedName("user")
	private User mUser;

	public Comment() {
	}

	public String getComment() {
		return mComment;
	}

	public Date getDatetime() {
		return mDatetime;
	}

	public User getUser() {
		return mUser;
	}

	@Override
	public String toString() {
		return "Comment{" +
				"mRemoteId=" + mRemoteId +
				", mComment='" + mComment + '\'' +
				", mDatetime='" + mDatetime.toString() + '\'' +
				", mUser=" + mUser +
				'}';
	}
}
