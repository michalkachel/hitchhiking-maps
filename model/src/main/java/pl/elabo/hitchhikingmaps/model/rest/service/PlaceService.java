package pl.elabo.hitchhikingmaps.model.rest.service;

import java.util.List;

import pl.elabo.hitchhikingmaps.model.entity.Place;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by michalkachel on 16.06.2015.
 */
public interface PlaceService {

	@GET("/")
	void getPlace(@Query("place") long id, Callback<Place> callback);

	@GET("/")
	void getPlaceBasic(@Query("place") long id, @Query("&dot") String dot, Callback<Place> callback);

	@GET("/")
	void getPlacesFromArea(@Query("bounds") String bounds, Callback<List<Place>> callback);

	@GET("/")
		//25.06.2015 not working on server side
	void getPlacesByCity(@Query("city") String cityName, Callback<List<Place>> callback);

	@GET("/")
	void getPlacesByCountryIso(@Query("country") String countryIso, Callback<List<Place>> callback);

	@GET("/")
	void getPlacesByContinent(@Query("continent") String continentCode, Callback<List<Place>> callback);


}
