package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 07.06.2015.
 */
public class User {
	@Expose
	@SerializedName("id")
	private long mRemoteId;
	@Expose
	@SerializedName("name")
	private String mName;

	public User() {
	}

	public String getName() {
		return mName;
	}

	@Override
	public String toString() {
		return "User{" +
				"mRemoteId=" + mRemoteId +
				", mName='" + mName + '\'' +
				'}';
	}
}
