package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by michalkachel on 07.06.2015.
 */

public class PlaceDescription {

	@Expose
	@SerializedName("datetime")
	private Date mDateTime;
	@Expose
	@SerializedName("fk_user")
	private Long mFkUser;
	@Expose
	@SerializedName("description")
	private String mDescription;
	@Expose
	@SerializedName("versions")
	private int mVersions;

	public PlaceDescription() {
	}

	public String getDescription() {
		return mDescription;
	}

	@Override
	public String toString() {
		return "PlaceDescription{" +
				"mDateTime='" + mDateTime + '\'' +
				", mFkUser=" + mFkUser +
				", mDescription='" + mDescription + '\'' +
				", mVersions=" + mVersions +
				'}';
	}
}
