package pl.elabo.hitchhikingmaps.model.rest.service;

import java.util.Map;

import pl.elabo.hitchhikingmaps.model.entity.Language;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by michalkachel on 16.06.2015.
 */
public interface LanguageService {

	@GET("/?languages")
	void getLanguages(Callback<Map<String, Language>> callback);

}
