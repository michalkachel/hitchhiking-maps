package pl.elabo.hitchhikingmaps.model.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by michalkachel on 17.07.2015.
 */
@Table(name = "plan_places")
public class PlanPlace extends Model {

	@Column(name = "place_remote_id", notNull = true, uniqueGroups = {"plan_place"}, onUniqueConflicts = {Column.ConflictAction.REPLACE})
	private long mPlaceRemoteId;
	@Column(name = "plan_id", notNull = true, uniqueGroups = {"plan_place"}, onUniqueConflicts = {Column.ConflictAction.REPLACE})
	private long mPlanId;

	public PlanPlace() {
	}

	public PlanPlace(long placeRemoteId, long planId) {
		mPlaceRemoteId = placeRemoteId;
		mPlanId = planId;
	}

	public long getPlaceRemoteId() {
		return mPlaceRemoteId;
	}
}
