package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 07.06.2015.
 */
public class WaitingStatistic {

	@Expose
	@SerializedName("avg")
	private int mAverage;
	@Expose
	@SerializedName("avg_textual")
	private String mAverageTextual;
	@Expose
	@SerializedName("count")
	private int mCount;
	@Expose
	@SerializedName("different_times")
	private int mDifferentTimesCount;

	public WaitingStatistic() {
	}

	public String getAverageTextual() {
		return mAverageTextual;
	}

	public int getDifferentTimesCount() {
		return mDifferentTimesCount;
	}

	@Override
	public String toString() {
		return "WaitingStatistic{" +
				"mAverage=" + mAverage +
				", mAverageTextual='" + mAverageTextual + '\'' +
				", mCount=" + mCount +
				", mDifferentTimesCount=" + mDifferentTimesCount +
				'}';
	}
}
