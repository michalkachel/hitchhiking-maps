package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 07.06.2015.
 */
public class Country {

	@Expose
	@SerializedName("iso")
	private String mIso;
	@Expose
	@SerializedName("name")
	private String mName;
	@Expose
	@SerializedName("places")
	private int mPlacesCount;
	@Expose
	@SerializedName("lat")
	private double mLatitude;
	@Expose
	@SerializedName("lon")
	private double mLongitude;

	public Country() {
	}

	public String getName() {
		return mName;
	}

	@Override
	public String toString() {
		return "Country{" +
				"mIso='" + mIso + '\'' +
				", mName='" + mName + '\'' +
				", mPlacesCount=" + mPlacesCount +
				", mLatitude=" + mLatitude +
				", mLongitude=" + mLongitude +
				'}';
	}
}
