package pl.elabo.hitchhikingmaps.model.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import pl.elabo.hitchhikingmaps.model.rest.service.ContinentService;
import pl.elabo.hitchhikingmaps.model.rest.service.CountryService;
import pl.elabo.hitchhikingmaps.model.rest.service.LanguageService;
import pl.elabo.hitchhikingmaps.model.rest.service.PlaceService;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class RestClient {

	private static final String BASE_URL = "http://hitchwiki.org/maps/api/";
	private static RestClient sRestClient;
	private Gson mGson;
	private PlaceService mPlaceService;
	private ContinentService mContinentService;
	private CountryService mCountryService;
	private LanguageService mLanguageService;

	RestClient() {
		mGson = new GsonBuilder()
				.setDateFormat("yyyy'-'MM'-'dd' 'HH':'mm':'ss")
				.excludeFieldsWithoutExposeAnnotation()
				.registerTypeAdapterFactory(new ResponseTypeAdapterFactory())
				.create();

		RestAdapter restAdapter = new RestAdapter.Builder()
				.setLogLevel(RestAdapter.LogLevel.NONE)
				.setEndpoint(BASE_URL)
				.setConverter(new GsonConverter(mGson))
				.build();

		mPlaceService = restAdapter.create(PlaceService.class);
		mContinentService = restAdapter.create(ContinentService.class);
		mCountryService = restAdapter.create(CountryService.class);
		mLanguageService = restAdapter.create(LanguageService.class);
	}

	public static RestClient getInstance() {
		if (sRestClient == null) {
			sRestClient = new RestClient();
		}

		return sRestClient;
	}

	public Gson getGson() {
		return mGson;
	}

	public PlaceService getPlaceService() {
		return mPlaceService;
	}

	public ContinentService getContinentService() {
		return mContinentService;
	}

	public CountryService getCountryService() {
		return mCountryService;
	}

	public LanguageService getLanguageService() {
		return mLanguageService;
	}
}