package pl.elabo.hitchhikingmaps.model.rest;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import retrofit.RetrofitError;

/**
 * Created by michalkachel on 06.07.2015.
 */
public class ResponseTypeAdapterFactory implements TypeAdapterFactory {

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

		final TypeAdapter<T> delegateTypeAdapter = gson.getDelegateAdapter(this, type);
		final TypeAdapter<JsonElement> elementTypeAdapter = gson.getAdapter(JsonElement.class);

		TypeAdapter<T> returnTypeAdapter = new TypeAdapter<T>() {
			@Override
			public void write(JsonWriter out, T value) throws IOException {
				delegateTypeAdapter.write(out, value);
			}

			@Override
			public T read(JsonReader in) throws IOException {
				JsonElement jsonElement = elementTypeAdapter.read(in);
				if (jsonElement.isJsonObject()) {
					JsonObject jsonObject = jsonElement.getAsJsonObject();
					if (jsonObject.has("error") && jsonObject.get("error").getAsBoolean() == true) {
						throw RetrofitError.unexpectedError(null, new Throwable(jsonObject.get("error_description").getAsString()));
					}
				}

				return delegateTypeAdapter.fromJsonTree(jsonElement);
			}
		};

		return returnTypeAdapter;

	}
}
