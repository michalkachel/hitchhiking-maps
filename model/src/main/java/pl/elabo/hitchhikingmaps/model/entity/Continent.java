package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 07.06.2015.
 */
public class Continent {

	@Expose
	@SerializedName("code")
	private String mCode;
	@Expose
	@SerializedName("name")
	private String mName;

	public Continent() {
	}

	public String getCode() {
		return mCode;
	}

	public String getName() {
		return mName;
	}

	@Override
	public String toString() {
		return "Continent{" +
				"mCode='" + mCode + '\'' +
				", mName='" + mName + '\'' +
				'}';
	}
}
