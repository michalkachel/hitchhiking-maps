package pl.elabo.hitchhikingmaps.model.rest.service;

import java.util.Map;

import pl.elabo.hitchhikingmaps.model.entity.Country;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by michalkachel on 16.06.2015.
 */
public interface CountryService {

	@GET("/?countries&coordinates")
	void getCountries(Callback<Map<String, Country>> callback);

}
