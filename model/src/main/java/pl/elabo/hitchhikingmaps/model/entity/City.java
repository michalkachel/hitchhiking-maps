package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 11.06.2015.
 */
public class City {

	@Expose
	@SerializedName("locality")
	private String mName;
	@Expose
	@SerializedName("country_iso")
	private String mCountryIso;
	@Expose
	@SerializedName("country_name")
	private String mCountryName;
	@Expose
	@SerializedName("places")
	private int mPlacesCount;

	public City() {
	}

	@Override
	public String toString() {
		return "City{" +
				"mName='" + mName + '\'' +
				", mCountryIso='" + mCountryIso + '\'' +
				", mCountryName='" + mCountryName + '\'' +
				", mPlacesCount=" + mPlacesCount +
				'}';
	}
}
