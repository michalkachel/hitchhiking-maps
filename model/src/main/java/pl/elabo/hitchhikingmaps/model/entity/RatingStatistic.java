package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by michalkachel on 07.06.2015.
 */
public class RatingStatistic {

	@Expose
	@SerializedName("ratings")
	private Map<Integer, Rating> mRatingsMap;
	@Expose
	@SerializedName("exact_rating")
	private float mExactRating;
	@Expose
	@SerializedName("rating_count")
	private int mRatingCount;
	@Expose
	@SerializedName("different_ratings")
	private int mDifferentRatingCount;

	public RatingStatistic() {
	}

	public int getRatingCount() {
		return mRatingCount;
	}

	@Override
	public String toString() {
		return "RatingStatistic{" +
				"mRatingsMap=" + mRatingsMap +
				", mExactRating=" + mExactRating +
				", mRatingCount=" + mRatingCount +
				", mDifferentRatingCount=" + mDifferentRatingCount +
				'}';
	}
}
