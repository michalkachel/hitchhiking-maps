package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 07.06.2015.
 */
public class PlaceLocationDescription {

	@Expose
	@SerializedName("locality")
	private String mLocality;
	@Expose
	@SerializedName("country")
	private Country mCountry;
	@Expose
	@SerializedName("continent")
	private Continent mContinent;

	public PlaceLocationDescription() {
	}

	public String getLocality() {
		return mLocality;
	}

	public Country getCountry() {
		return mCountry;
	}

	public Continent getContinent() {
		return mContinent;
	}

	@Override
	public String toString() {
		return "PlaceLocationDescription{" +
				"mLocality='" + mLocality + '\'' +
				", mCountry=" + mCountry +
				", mContinent=" + mContinent +
				'}';
	}
}
