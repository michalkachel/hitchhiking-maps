package pl.elabo.hitchhikingmaps.model.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michalkachel on 17.07.2015.
 */
@Table(name = "plans")
public class Plan extends Model {

	@Column(name = "name", notNull = true)
	private String mName;

	private List<Place> mPlaces = new ArrayList<>();

	public Plan() {
	}

	public Plan(String name) {
		mName = name;
	}

	public void saveWithPlaces() {
		for (Place place : mPlaces) {
			PlanPlace planPlace = new PlanPlace(place.getRemoteId(), this.getId());
			planPlace.save();
		}
		save();
	}

	public void deleteWithPlaces() {
		deletePlanPlaces();
		delete();
	}

	private void deletePlanPlaces() {
		new Delete().from(PlanPlace.class).where("plan_id = ?", this.getId()).execute();
	}

	public void loadPlaces() {
		mPlaces.clear();
		for (PlanPlace planPlace : getPlanPlaces()) {
			mPlaces.add(Place.loadByRemoteId(planPlace.getPlaceRemoteId()));
		}
	}

	private List<PlanPlace> getPlanPlaces() {
		return new Select().from(PlanPlace.class).where("plan_id = ?", this.getId()).execute();
	}

	public String getName() {
		return mName;
	}

	public List<Place> getPlaces() {
		if (mPlaces.size() == 0) {
			loadPlaces();
		}
		return mPlaces;
	}

	@Override
	public String toString() {
		return mName;
	}

	public int getPlacesCount() {
		return getPlaces().size();
	}

	public boolean isMapDownloaded() {
		loadPlaces();
		for (Place place : getPlaces()) {
			if (!place.isMapDownloaded()) return false;
		}
		return true;
	}
}
