package pl.elabo.hitchhikingmaps.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by michalkachel on 11.06.2015.
 */
public class Language {

	@Expose
	@SerializedName("code")
	private String mCode;
	@Expose
	@SerializedName("name")
	private String mName;
	@Expose
	@SerializedName("name_original")
	private String mNameOriginal;

	public Language() {
	}

	@Override
	public String toString() {
		return "Language{" +
				"mCode='" + mCode + '\'' +
				", mName='" + mName + '\'' +
				", mNameOriginal='" + mNameOriginal + '\'' +
				'}';
	}
}
